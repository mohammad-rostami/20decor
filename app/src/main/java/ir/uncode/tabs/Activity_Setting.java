package ir.uncode.tabs;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Poweruser on 08/10/2016.
 */
public class Activity_Setting extends Activity {

    private SharedPreferences.Editor idPrefEdit;
    private SharedPreferences.Editor boolPrefEdit;
    private SharedPreferences.Editor settingEdit;
    private ImageView btnBack;
    private ImageView btn_like;
    private ImageView btn_in_room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        btnBack = (ImageView) findViewById(R.id.btnBack);
        btn_like = (ImageView) findViewById(R.id.btn_like);
        btn_in_room = (ImageView) findViewById(R.id.btn_in_room);
        btn_like.setVisibility(View.GONE);
        btn_in_room.setVisibility(View.GONE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Switch price_switch = (Switch) findViewById(R.id.price_switch);
        final Switch producer_switch = (Switch) findViewById(R.id.producer_switch);
        TextView price_switch_text = (TextView) findViewById(R.id.price_switch_text);
        TextView producer_switch_text = (TextView) findViewById(R.id.producer_switch_text);
        price_switch_text.setTypeface(Activity_Main.SANS);
        producer_switch_text.setTypeface(Activity_Main.SANS);


        idPrefEdit = G.pricePref.edit();
        boolPrefEdit = G.producerPref.edit();
        settingEdit=G.settingPref.edit();

        Boolean x = G.pricePref.getBoolean("price_data", true);
        if (x) {
            price_switch.setChecked(true);
        } else {
            price_switch.setChecked(false);
        }

        Boolean m = G.producerPref.getBoolean("producer_data", true);
        if (m) {
            producer_switch.setChecked(true);
        } else {
            producer_switch.setChecked(false);
        }


        price_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (price_switch.isChecked()) {
                    idPrefEdit.putBoolean("price_data", true);
                    idPrefEdit.commit();
                } else if (!price_switch.isChecked()) {
                    idPrefEdit.putBoolean("price_data", false);
                    idPrefEdit.commit();
                }
            }
        });
        producer_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (producer_switch.isChecked()) {
                    boolPrefEdit.putBoolean("producer_data", true);
                    boolPrefEdit.commit();
                } else if (!producer_switch.isChecked()) {
                    boolPrefEdit.putBoolean("producer_data", false);
                    boolPrefEdit.commit();
                }
            }
        });


    }
}
