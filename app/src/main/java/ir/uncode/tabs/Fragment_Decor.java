package ir.uncode.tabs;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;


public class Fragment_Decor extends Fragment {
    public ArrayList<Struct> structs = new ArrayList<>();
    private int Tab = 4;
    public static String name;

    public Fragment_Decor() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);

        void addTab(TabLayout.Tab tab);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            JSONObject manufacturer = Splash_Screen.OBJECTS.getJSONObject("look");
            JSONArray data = manufacturer.getJSONArray("data");

            for (int i = 0; i < data.length(); i++) {
                Struct struct = new Struct();
                JSONObject items = data.getJSONObject(i);
                struct.strFirst = "nothing";
                struct.strSecond = "nothing";
                struct.image = items.getString("image");
                struct.strThird = items.getString("target");
                structs.add(struct);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        RecyclerView.Adapter adapter = new Adapter_Recycler(G.context,structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);

        return rootView;
    }

    private class Async2 extends Webservice.PostClass {


        @Override
        protected void onPreExecute() {
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            Activity_Main.request = result;
        }
    }


}