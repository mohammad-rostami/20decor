package ir.uncode.tabs;

public interface OnItemListener {
    public void onItemSelect(int position);
    public void onItemClick(int position);
}
