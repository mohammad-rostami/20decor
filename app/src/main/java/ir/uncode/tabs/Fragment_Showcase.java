package ir.uncode.tabs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;


public class Fragment_Showcase extends Fragment {
    public ArrayList<Struct> structs = new ArrayList<>();
    public ArrayList<Struct> structCat = new ArrayList<>();
    private int Tab = 9;
    public static Context mContext;
    public static String name;

    public Fragment_Showcase() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);

        void addTab(TabLayout.Tab tab);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();


        for (int i = 0; i < Splash_Screen.BANNERS.size(); i++) {
            Struct struct = new Struct();
            struct.strFirst = "آیتم" + i;
            struct.image = String.valueOf(Splash_Screen.BANNERS.get(i));
            struct.strThird = String.valueOf(Splash_Screen.TARGETS.get(i));
            structs.add(struct);
        }
        JSONArray categories = Splash_Screen.cat_array;
        for (int i = 0; i < categories.length(); i++) {
            try {
                JSONObject cat_items = categories.getJSONObject(i);
                Struct struct = new Struct();
                struct.strFirst = cat_items.getString("name");
                struct.image = cat_items.getString("image");
                struct.strThird = cat_items.getString("target");
                structCat.add(struct);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_showcase, container, false);
        ImageView img = (ImageView) rootView.findViewById(R.id.top_banner);
        TextView txt_cat = (TextView) rootView.findViewById(R.id.txt_cat);


        txt_cat.setTypeface(Activity_Main.SANS);
        Glide
                .with(Activity_Main.CONTEXT)
                .load(Splash_Screen.s)
                .placeholder(R.drawable.background_card)
                .centerCrop()
                .into(img);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);
        RecyclerView.Adapter adapter = new Adapter_Recycler(G.context, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, 2, false);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);

        RecyclerView recyclerViewCat = (RecyclerView) rootView.findViewById(R.id.recyclerView_cat);
        recyclerViewCat.setNestedScrollingEnabled(false);
        RecyclerView.Adapter adapterCat = new Adapter_Recycler(G.context, structCat, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structCat.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);
        recyclerViewCat.setAdapter(adapterCat);
        RecyclerView.LayoutManager managerCat = new GridLayoutManager(getContext(), 4);
        recyclerViewCat.setLayoutManager(managerCat);
        recyclerViewCat.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        return rootView;
    }



}