package ir.uncode.tabs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Fragment_manufacturer_products extends Fragment {
    public ArrayList<Struct> structs = new ArrayList<>();
    private int Tab = 5;
    public static String name;
    private boolean isGrid;
    private LinearLayoutManager manager;
    private JSONArray firstArray;
    private ProgressDialog progress;
    private ProgressDialog progress1;
    private RecyclerView.Adapter adapter;
    private TextView section_label;


    public Fragment_manufacturer_products() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);

        void addTab(TabLayout.Tab tab);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        progress = new ProgressDialog(getContext(), R.style.MyTheme);
        progress.setCancelable(false);
        progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
        progress.show();
        new Async2().execute(Activity_Main.target_cat, G.target, G.target_code, String.valueOf(1));
//        request_results = (TextView) findViewById(R.id.request_results);
//        request_results.setTypeface(Activity_Main.SANS);
//        try {
//            JSONObject manufacturer = Splash_Screen.OBJECTS.getJSONObject("look");
//            JSONArray data = manufacturer.getJSONArray("data");
//
//            for (int i = 0; i < data.length(); i++) {
//                Struct struct = new Struct();
//                JSONObject items = data.getJSONObject(i);
//                struct.strFirst = "nothing";
//                struct.strSecond = "nothing";
//                struct.image = items.getString("image");
//                struct.strThird = items.getString("target");
//                structs.add(struct);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_manufacturer_products, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.product_list);
        section_label = (TextView) rootView.findViewById(R.id.section_label);
        section_label.setTypeface(Activity_Main.SANS);
        adapter = new Adapter_Recycler(G.context, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, isGrid);

        //Is Grid
        if (isGrid) {
            manager = new LinearLayoutManager(G.context);

        } else {
            manager = new GridLayoutManager(G.context, 2);

        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.removeItemDecoration(null);
        recyclerView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                customLoadMoreDataFromApi(page);
            }
        });


        return rootView;
    }

    private void progressBar() {
        progress.dismiss();

    }

    private class Async2 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar();

//            Activity_Main.request = result;
            try {
                JSONObject data = new JSONObject(result);
                JSONObject firstObject = data.getJSONObject("data");
                firstArray = firstObject.getJSONArray("products");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strSecond = secondObject.getString("manufacturer");
                    struct.image = secondObject.getString("thumb");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    String final_price_checker = secondObject.get("special").toString();
                    if (final_price_checker.equals("false")) {
                    } else {
                        struct.strChangedPrice = secondObject.get("special").toString();
                    }
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
//                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();
            if (structs.size() == 0) {
                section_label.setText("فعلا اینجا هیچی نیست!");
            } else {
                section_label.setVisibility(View.GONE);
            }
        }
    }

    private class Async3 extends Webservice.PostClass {


        @Override
        protected void onPreExecute() {
//            progress1 = ProgressDialog.show(Activity_ProductsList.this, "", "Loading...", false, true);
//            progress1 = new ProgressDialog(Activity_Main.CONTEXT, R.style.MyTheme);
//            progress1.setCancelable(false);
//            progress1.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
//            progress1.show();
        }

        @Override
        protected void onPostExecute(String result) {
//            progress1.dismiss();

            try {
                JSONObject data = new JSONObject(result);
                JSONObject firstObject = data.getJSONObject("data");
                firstArray = firstObject.getJSONArray("products");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strSecond = secondObject.getString("manufacturer");
                    struct.image = secondObject.getString("thumb");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    String final_price_checker = secondObject.get("special").toString();
                    if (final_price_checker.equals("false")) {
                    } else {
                        struct.strChangedPrice = secondObject.get("special").toString();
                    }
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();

        }
    }

    public void customLoadMoreDataFromApi(int page) {
//        Toast.makeText(Activity_Main.CONTEXT, String.valueOf(page + 1), Toast.LENGTH_SHORT).show();
        new Async3().execute("product_list", G.target, G.target_code, String.valueOf(page + 1));
    }

}