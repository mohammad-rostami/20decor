package ir.uncode.tabs;

/**
 * Created by Poweruser on 10/07/2016.
 */
public interface Async_Response {
    void getProcessFinish(String output);
    void postProcessFinish(String output);
}
