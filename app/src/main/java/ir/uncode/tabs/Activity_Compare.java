package ir.uncode.tabs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.idunnololz.widgets.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Activity_Compare extends FragmentActivity {
    private RecyclerView.LayoutManager manager;
    private RecyclerView.Adapter adapter;
    private TextView request_results;
    private ProgressDialog progress1;
    public String word;
    Integer myNum = 1;
    ImageView btnBack;
    public static String name;
    private ProgressDialog progress;
    private Intent intent;

    private int Tab = 6;
    private ArrayList<Struct> structs = new ArrayList<>();
    private boolean isGrid;

    private JSONArray firstArray;
    private JSONArray attribArray;
    private JSONObject firstObject;
    private static ViewPager mViewPager;
    private AnimatedExpandableListView listView;
    private ExampleAdapter adapterExp;
    private static ArrayList mImageSlider = new ArrayList();
    List<GroupItem> items;
    TextView txt_model;
    TextView txt1;
    private LinearLayout price_layout;
    TextView txt_price;
    TextView txt_manufacturer;
    ImageView first_product_image;

    private JSONArray firstArray_com;
    private JSONArray attribArray_com;
    private JSONObject firstObject_com;
    private static ViewPager mViewPager_com;
    private AnimatedExpandableListView listView_com;
    private ExampleAdapter adapterExp_com;
    private static ArrayList mImageSlider_com = new ArrayList();
    List<GroupItem> items_com;
    TextView txt_model_com;
    TextView txt1_com;
    TextView txt3;
    TextView txt3_com;
    private LinearLayout price_layout_com;
    TextView txt_price_com;
    TextView txt_manufacturer_com;
    static LinearLayout search_view;
    ImageView second_product_image;
    private JSONArray other_products;
    private ArrayList<Struct> relatedStructs = new ArrayList<>();
    private RecyclerView.Adapter adapter_related;
    private RecyclerView recyclerViewRelated;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mImageSlider.clear();
        mImageSlider_com.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare);
        mImageSlider.clear();

        txt_model = (TextView) findViewById(R.id.txt_model);
        txt_model.setTypeface(Activity_Main.SANS);
        price_layout = (LinearLayout) findViewById(R.id.price_layout);
        txt_price = (TextView) findViewById(R.id.txt_price);
        txt_price.setTypeface(Activity_Main.SANS);
        txt1 = (TextView) findViewById(R.id.txt1);
        txt1.setTypeface(Activity_Main.SANS);
        txt3 = (TextView) findViewById(R.id.txt3);
        txt3.setTypeface(Activity_Main.SANS);
        txt3_com = (TextView) findViewById(R.id.txt3_com);
        txt3_com.setTypeface(Activity_Main.SANS);
        txt_manufacturer = (TextView) findViewById(R.id.txt_manufacturer);
        txt_manufacturer.setTypeface(Activity_Main.SANS);
        first_product_image = (ImageView) findViewById(R.id.first_product_image);

        txt_model_com = (TextView) findViewById(R.id.txt_model_com);
        txt_model_com.setTypeface(Activity_Main.SANS);
        price_layout_com = (LinearLayout) findViewById(R.id.price_layout_com);
        txt_price_com = (TextView) findViewById(R.id.txt_price_com);
        txt_price_com.setTypeface(Activity_Main.SANS);
        txt1_com = (TextView) findViewById(R.id.txt1_com);
        txt1_com.setTypeface(Activity_Main.SANS);
        txt_manufacturer_com = (TextView) findViewById(R.id.txt_manufacturer_com);
        txt_manufacturer_com.setTypeface(Activity_Main.SANS);
        search_view = (LinearLayout) findViewById(R.id.search_view);
        second_product_image = (ImageView) findViewById(R.id.second_product_image);
        ImageView btn_like = (ImageView) findViewById(R.id.btn_like);
        btn_like.setVisibility(View.GONE);
        ImageView btn_in_room = (ImageView) findViewById(R.id.btn_in_room);
        btn_in_room.setVisibility(View.GONE);

        ImageView new_product = (ImageView) findViewById(R.id.new_product);
        LinearLayout new_product_layout = (LinearLayout) findViewById(R.id.new_product_layout);

        new_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_view.setVisibility(View.VISIBLE);
                if (structs.size()==0){
                    recyclerViewRelated.setVisibility(View.VISIBLE);
                }
            }
        });

        btnBack = (ImageView) findViewById(R.id.btnBack);
        btnBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_black_24dp));

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//first compare page content
        try {
            firstObject = Activity_Products.firstObject;
            JSONObject secondObject = firstObject.getJSONObject("manufacturer_info");
            attribArray = firstObject.getJSONArray("attribute_groups");
            firstArray = firstObject.getJSONArray("images");
            for (int i = 0; i < firstArray.length(); i++) {
                JSONObject items = firstArray.getJSONObject(i);
                String imageSlider = items.getString("popup");
                mImageSlider.add(imageSlider);
            }

            String productName = firstObject.getString("name");
            String price = firstObject.getString("price");
            String manufacturerName = secondObject.getString("name");

            txt_model.setText(productName);
            txt_manufacturer.setText(manufacturerName);
            txt_price.setText(price);
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(mImageSlider.get(0))
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .into(first_product_image);

            if (txt_price.getText().toString().equals("false")) {
                price_layout.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Related Items (recyclerView)
        try {
            other_products = firstObject.getJSONArray("related_products");
//            String relatedChecker = firstObject.get("related_products").toString();
//            if (relatedChecker.equals("false")) {
//                Toast.makeText(Activity_Compare.this, "no related", Toast.LENGTH_SHORT).show();
//
////                relativeTopic.setVisibility(View.GONE);
////                relativeLayout.setVisibility(View.GONE);
////                no_related.setText("محصول مشابه پیدا نشد!");
//            } else {
////                no_related.setVisibility(View.GONE);

            for (int l = 0; l < other_products.length(); l++) {
                Struct struct = new Struct();
                JSONObject relatedProducts = other_products.getJSONObject(l);
                struct.strFirst = relatedProducts.getString("name");
                struct.strSecond = relatedProducts.getString("price");
                struct.strThird = relatedProducts.getString("target");
                struct.image = relatedProducts.getString("thumb");
                relatedStructs.add(struct);
//                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        recyclerViewRelated = (RecyclerView) findViewById(R.id.related_list);
        adapter_related = new Adapter_Recycler(this, relatedStructs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = relatedStructs.get(position).strThird;
                G.target = "";
                G.target_code = "";
                G.target = name.substring(0, name.indexOf("::"));
                G.target_code = name.substring(name.indexOf("::") + 2, name.length());
//                Toast.makeText(Activity_Compare.this, G.target_code, Toast.LENGTH_SHORT).show();
                switch (G.target) {
                    case "category_info":
                        Activity_Main.target_cat = "category_info";
                        break;
                    case "category_list":
                        Activity_Main.target_cat = "category_list";
                        break;
                    case "manufacturer_info":
                        Activity_Main.target_cat = "manufacturer_info";
                        break;
                    case "manufacturer_list":
                        Activity_Main.target_cat = "manufacturer_list";
                        break;
                    case "product_list":
                        Activity_Main.target_cat = "product_list";
                        break;
                    case "product_info":
                        Activity_Main.target_cat = "product_info";
                        break;
                    case "custom":
                        Activity_Main.target_cat = "custom";
                        break;
                    case "information_info":
                        Activity_Main.target_cat = "information_info";
                        break;
                    case "look_info":
                        Activity_Main.target_cat = "look_info";
                        break;
                    case "look_list":
                        Activity_Main.target_cat = "look_list";
                        break;
                }
                new Async2().execute(Activity_Main.target_cat, G.target, G.target_code, String.valueOf(myNum));
                search_view.setVisibility(View.GONE);
                recyclerViewRelated.setVisibility(View.GONE);
                if (mImageSlider_com.size() > 0) {
                    mImageSlider_com.clear();
                }
            }

            @Override
            public void onItemClick(int position) {

            }
        }, 11, false);

        RecyclerView.LayoutManager manager1 = new LinearLayoutManager(getApplicationContext());
        recyclerViewRelated.setAdapter(adapter_related);
        recyclerViewRelated.setLayoutManager(manager1);
        recyclerViewRelated.removeItemDecoration(null);


//first compare Expandable ListView (onCreate)
        items = new ArrayList<GroupItem>();
        try {
            for (int i = 0; i < attribArray.length(); i++) {
                JSONObject attribs = attribArray.getJSONObject(i);
                GroupItem item = new GroupItem();
                item.title = attribs.getString("name");
                JSONArray attribsChild = attribs.getJSONArray("attribute");

                for (int j = 0; j < attribsChild.length(); j++) {
                    JSONObject childs = attribsChild.getJSONObject(j);
                    ChildItem child = new ChildItem();
                    child.title = childs.getString("name");
                    child.hint = childs.getString("text");
                    item.items.add(child);
                }
                items.add(item);
            }
            adapterExp = new ExampleAdapter(this);
            adapterExp.setData(items);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        listView = (AnimatedExpandableListView) findViewById(R.id.attrib);
        listView.setGroupIndicator(null);
        listView.setChildIndicator(null);
        listView.setDividerHeight(2);
        listView.setAdapter(adapterExp);
        for (int k = 0; k < attribArray.length(); k++) {
            listView.expandGroup(k);
        }
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (listView.isGroupExpanded(groupPosition)) {
//                    listView.collapseGroupWithAnimation(groupPosition);
                } else {
//                    listView.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }
        });
        Utility.setListViewHeightBasedOnChildren(listView);

// Search part (on Create)
        final EditText search_box = (EditText) findViewById(R.id.search_box);
        search_box.setTypeface(Activity_Main.SANS);
        ImageView search_btn = (ImageView) findViewById(R.id.search_btn);
        search_box.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    word = search_box.getText().toString();
                    Activity_Main.target_cat = "search";
                    new Async1().execute(Activity_Main.target_cat, "search", word, String.valueOf(1));

                    progress = new ProgressDialog(Activity_Compare.this, R.style.MyTheme);
                    progress.setCancelable(true);
                    progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
                    progress.show();

                    recyclerViewRelated.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });
        search_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                word = search_box.getText().toString();
                Activity_Main.target_cat = "search";
                new Async1().execute(Activity_Main.target_cat, "search", word, String.valueOf(1));

                progress = new ProgressDialog(Activity_Compare.this, R.style.MyTheme);
                progress.setCancelable(true);
                progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
                progress.show();

                recyclerViewRelated.setVisibility(View.GONE);
            }
        });
        initializeRecyclerView(structs, !isGrid);
        request_results = (TextView) findViewById(R.id.request_results);
        request_results.setTypeface(Activity_Main.SANS);
    }

    // Search Part
    private void initializeRecyclerView(final ArrayList<Struct> structs, boolean isGrid) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.product_list);
        adapter = new Adapter_Recycler(this, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                G.target = "";
                G.target_code = "";
                G.target = name.substring(0, name.indexOf("::"));
//                Toast.makeText(Activity_Compare.this, G.target, Toast.LENGTH_SHORT).show();
                G.target_code = name.substring(name.indexOf("::") + 2, name.length());
                switch (G.target) {
                    case "category_info":
                        Activity_Main.target_cat = "category_info";
                        break;
                    case "category_list":
                        Activity_Main.target_cat = "category_list";
                        break;
                    case "manufacturer_info":
                        Activity_Main.target_cat = "manufacturer_info";
                        break;
                    case "manufacturer_list":
                        Activity_Main.target_cat = "manufacturer_list";
                        break;
                    case "product_list":
                        Activity_Main.target_cat = "product_list";
                        break;
                    case "product_info":
                        Activity_Main.target_cat = "product_info";
                        break;
                    case "custom":
                        Activity_Main.target_cat = "custom";
                        break;
                    case "information_info":
                        Activity_Main.target_cat = "information_info";
                        break;
                    case "look_info":
                        Activity_Main.target_cat = "look_info";
                        break;
                    case "look_list":
                        Activity_Main.target_cat = "look_list";
                        break;
                }
                new Async2().execute(Activity_Main.target_cat, G.target, G.target_code, String.valueOf(myNum));
                search_view.setVisibility(View.GONE);
                if (mImageSlider_com.size() > 0) {
                    mImageSlider_com.clear();
                }
            }

            @Override
            public void onItemClick(int position) {

            }
        }, 10, isGrid);
        manager = new LinearLayoutManager(getApplicationContext());
        //Is Grid
        if (isGrid) {

        } else {
            manager = new LinearLayoutManager(getApplicationContext());
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.removeItemDecoration(null);
        recyclerView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                customLoadMoreDataFromApi(page);
            }
        });
        //Save isGrid Value
        this.isGrid = isGrid;
    }

    // Post method Acync1
    private class Async1 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            structs.clear();
            adapter.notifyDataSetChanged();
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar();
            try {
                JSONObject data = new JSONObject(result);
                JSONArray firstArray = data.getJSONArray("results");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    struct.image = secondObject.getString("image");
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();
            if (structs.size()==0){
                request_results.setVisibility(View.VISIBLE);
                request_results.setText("هیچ محصولی پیدا نشد!");
            }else {
                request_results.setVisibility(View.GONE);
            }
        }
    }

    // Post method Acync3
    private class Async3 extends Webservice.PostClass {
        @Override
        protected void onPreExecute() {
            progress1 = new ProgressDialog(Activity_Compare.this, R.style.MyTheme);
            progress1.setCancelable(true);
            progress1.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress1.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress1.dismiss();

            try {
                JSONObject data = new JSONObject(result);
                JSONArray firstArray = data.getJSONArray("results");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    struct.image = secondObject.getString("image");
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();
        }
    }

    private void progressBar() {
        progress.dismiss();
    }

    public void customLoadMoreDataFromApi(int page) {
//        Toast.makeText(Activity_Compare.this, String.valueOf(page + 1), Toast.LENGTH_SHORT).show();
        new Async3().execute(Activity_Main.target_cat, "search", word, String.valueOf(page + 1));
    }

//first Expandable listView

    private static class GroupItem {
        String title;
        List<ChildItem> items = new ArrayList<ChildItem>();
        List<ChildItem> items_com = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
    }

    private static class ChildHolder {
        TextView title;
        TextView hint;
    }

    private static class GroupHolder {
        TextView title;
    }

    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.expandable_productchild, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.hint = (TextView) convertView.findViewById(R.id.textHint);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.hint.setText(item.hint);
            holder.title.setTypeface(Activity_Main.SANS);
            holder.hint.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public int getPositionForSection(int section) {
            return listView.getFlatListPosition(ExpandableListView
                    .getPackedPositionForGroup(section));
        }

        @Override
        public int getSectionForPosition(int position) {
            return ExpandableListView.getPackedPositionGroup(listView
                    .getExpandableListPosition(position));
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.expandable_productgroup, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }

    private class Async2 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String result) {
//            progress.dismiss();
//            Activity_Main.request = result;
            // Page Contents
            try {
                JSONObject data = new JSONObject(result);
                firstObject_com = data.getJSONObject("data");
                JSONObject secondObject = firstObject_com.getJSONObject("manufacturer_info");
                attribArray_com = firstObject_com.getJSONArray("attribute_groups");

                String f = String.valueOf(attribArray_com.length());
//                Toast.makeText(Activity_Compare.this, f, Toast.LENGTH_SHORT).show();

                firstArray_com = firstObject_com.getJSONArray("images");
                for (int i = 0; i < firstArray_com.length(); i++) {
                    JSONObject items = firstArray_com.getJSONObject(i);
                    String imageSlider = items.getString("popup");
                    mImageSlider_com.add(imageSlider);
                    Glide
                            .with(Activity_Main.CONTEXT)
                            .load(mImageSlider_com.get(0))
                            .placeholder(R.drawable.no)
                            .fitCenter()
                            .into(second_product_image);
                }
                String productName = firstObject_com.getString("name");
                String price = firstObject_com.getString("price");
                String manufacturerName = secondObject.getString("name");

                txt_model_com.setText(productName);
                txt_manufacturer_com.setText(manufacturerName);
                txt_price_com.setText(price);
                if (txt_price_com.getText().toString().equals("false")) {
                    price_layout_com.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
//                Toast.makeText(Activity_Compare.this, "sdadsa", Toast.LENGTH_SHORT).show();
            }
//            if (structs.size()==0){
//                request_results.setText("هیچ محصولی پیدا نشد!");
//            }else {
//                request_results.setVisibility(View.GONE);
//            }


            items = new ArrayList<GroupItem>();
            try {
                for (int i = 0; i < attribArray_com.length(); i++) {
                    JSONObject attribs = attribArray_com.getJSONObject(i);
                    GroupItem item = new GroupItem();
                    item.title = "";
                    JSONArray attribsChild = attribs.getJSONArray("attribute");

                    for (int j = 0; j < attribsChild.length(); j++) {
                        JSONObject childs = attribsChild.getJSONObject(j);
                        ChildItem child = new ChildItem();
                        child.title = "";
                        child.hint = childs.getString("text");
                        item.items.add(child);
                    }
                    items.add(item);
                }
                adapterExp = new ExampleAdapter(Activity_Compare.this);
                adapterExp.setData(items);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            listView_com = (AnimatedExpandableListView) findViewById(R.id.attrib_com);
            listView_com.setGroupIndicator(null);
            listView_com.setChildIndicator(null);
            listView_com.setDividerHeight(2);
            listView_com.setAdapter(adapterExp);
            for (int k = 0; k < attribArray_com.length(); k++) {
                listView_com.expandGroup(k);
            }
            listView_com.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    if (listView_com.isGroupExpanded(groupPosition)) {
//                        listView_com.collapseGroupWithAnimation(groupPosition);
                    } else {
//                        listView_com.expandGroupWithAnimation(groupPosition);
                    }
                    return true;
                }

            });
            adapterExp.notifyDataSetChanged();
            Utility.setListViewHeightBasedOnChildren(listView_com);

        }

    }

    private void expand() {
        int sa = attribArray_com.length();
        for (int k = 0; k < attribArray_com.length(); k++) {
            listView_com.expandGroup(k);
        }
    }

}
