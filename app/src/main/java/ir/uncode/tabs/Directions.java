package ir.uncode.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.StringTokenizer;


public class Directions extends Activity {
    private static Context context;
    private static Intent intent;

    public Directions(Context context) {
        this.context = context;
    }

    public static void checkActivity(String string) {

        G.target = "";
        G.target_code = "";
        G.target = string.substring(0,string.indexOf("::"));
        G.target_code = string.substring(string.indexOf("::") + 2,string.length());

//        Toast.makeText(G.currentActivity, G.target_code, Toast.LENGTH_SHORT).show();

        switch (G.target) {
            case "category_info":
                Activity_Main.target_cat = "category_info";
                intent = new Intent(G.currentActivity, Activity_ProductsList.class);
                G.currentActivity.startActivity(intent);
                break;
            case "category_list":
                Activity_Main.target_cat = "category_list";
                intent = new Intent(G.currentActivity, Activity_ProductsList.class);
                G.currentActivity.startActivity(intent);
                break;
            case "manufacturer_info":
                Activity_Main.target_cat = "manufacturer_info";
                intent = new Intent(G.currentActivity,Activity_Manufacturer.class);
                G.currentActivity.startActivity(intent);
                break;
            case "manufacturer_list":
                Activity_Main.target_cat = "manufacturer_list";
                intent = new Intent(G.currentActivity, Activity_ProductsList.class);
                G.currentActivity.startActivity(intent);
                break;
            case "product_list":
                Activity_Main.target_cat = "product_list";
                intent = new Intent(G.currentActivity, Activity_ProductsList.class);
                G.currentActivity.startActivity(intent);
                break;
            case "product_info":
                Activity_Main.target_cat = "product_info";
                intent = new Intent(G.currentActivity, Activity_Products.class);
                G.currentActivity.startActivity(intent);
                break;
            case "custom":
                Activity_Main.target_cat = "custom";
                intent = new Intent(G.currentActivity, Activity_Information.class);
                G.currentActivity.startActivity(intent);
                break;
            case "information_info":
                Activity_Main.target_cat = "information_info";
                intent = new Intent(G.currentActivity, Activity_Information.class);
                G.currentActivity.startActivity(intent);
                break;
            case "look_info":
                Activity_Main.target_cat = "look_info";
                intent = new Intent(G.currentActivity, Activity_ProductsList.class);
                G.currentActivity.startActivity(intent);
                break;
            case "look_list":
                Activity_Main.target_cat = "look_list";
                intent = new Intent(G.currentActivity, Activity_ProductsList.class);
                G.currentActivity.startActivity(intent);
                break;
        }
    }
}
