package ir.uncode.tabs;


import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class Activity_Information extends Activity {
    private TextView pageName;
    private WebView desc;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        ImageView btnNavigation = (ImageView) findViewById(R.id.btnNavigation);
        ImageView btnSort = (ImageView) findViewById(R.id.btnSort);
        pageName = (TextView) findViewById(R.id.pageName);
        pageName.setText(Activity_Main.groupTitle);
        desc = (WebView) findViewById(R.id.desc);
        desc.getSettings().setJavaScriptEnabled(true);
        desc.getSettings().setDomStorageEnabled(true);
        desc.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        desc.getSettings().setPluginState(WebSettings.PluginState.ON);
        pageName.setTypeface(Activity_Main.SANS);
        btnSort.setVisibility(View.GONE);
        btnNavigation.setImageResource(R.drawable.icon_back_arrow);

        progress = new ProgressDialog(Activity_Information.this, R.style.MyTheme);
        progress.setCancelable(false);
        progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
        progress.show();

        desc.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                progress.show();

            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                prDialog.dismiss();
                progress.dismiss();

            }
        });
        String s = G.target_code;
        if (G.target.equals("custom")) {
            desc.loadUrl(G.target_code);
        } else if (G.target !="custom"){
            new Async2().execute(Activity_Main.target_cat, G.target, G.target_code, String.valueOf(1));
            btnNavigation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

    }

    private class Async2 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String result) {

            JSONObject info = null;
            try {
                info = new JSONObject(result);
                JSONObject data = info.getJSONObject("data");
                String description = data.getString("description");
                desc.loadData("<html dir=\"rtl\" lang=\"\"><body>" + description + "</body></html>", "text/html; charset=utf-8", "UTF-8");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
