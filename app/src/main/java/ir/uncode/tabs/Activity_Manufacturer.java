package ir.uncode.tabs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.karim.MaterialTabs;

/**
 * Created by Poweruser on 13/09/2016.
 */
public class Activity_Manufacturer extends AppCompatActivity implements Fragment_manufacturer_data.OnFragmentInteractionListener, Fragment_manufacturer_products.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener {
    private int Tab = 5;
    private ArrayList<Struct> structs = new ArrayList<>();
    private JSONArray firstArray;
    private boolean isGrid;
    private RecyclerView.LayoutManager manager;
    private RecyclerView.Adapter adapter;
    private ImageView btnSort;
    private static DrawerLayout drawer;
    private TextView request_results;
    private ProgressDialog progress;
    private ProgressDialog progress1;
    public static String name;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private Activity activity;
    private boolean added;
    private TextView tabText;
    public static String target;
    public static String targets;
    public static String target_code;
    private ImageView cover;
    private TextView manufacturer_tell;
    static boolean active = false;

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manufacturer);
        progress = new ProgressDialog(Activity_Manufacturer.this, R.style.MyTheme);
        progress.setCancelable(false);
        progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
        progress.show();
        if (G.isUpdate) {
            new Async3().execute("manufacturer_info", G.target, G.target_code, String.valueOf(1));
            G.isUpdate = false;
        } else {
            targets = "";
            target_code = "";
            targets = target.substring(0, target.indexOf("::"));
            target_code = target.substring(target.indexOf("::") + 2, target.length());
            new Async3().execute("manufacturer_info", targets, target_code, String.valueOf(1));
        }


// Sort
        cover = (ImageView) findViewById(R.id.cover);
        btnSort = (ImageView) findViewById(R.id.btnSort);
        btnSort.setVisibility(View.GONE);
        TextView pageName = (TextView) findViewById(R.id.pageName);
        pageName.setText("صفحه تولیده کننده");
        pageName.setTypeface(Activity_Main.SANS);

// Navigation Drawer
        ImageView btnMenu = (ImageView) findViewById(R.id.btnNavigation);
        TextView manufacturer_name = (TextView) findViewById(R.id.manufacturer_name);
        manufacturer_tell = (TextView) findViewById(R.id.manufacturer_tell);

        manufacturer_name.setText(Activity_Main.name);
        manufacturer_name.setTypeface(Activity_Main.SANS);
        manufacturer_tell.setTypeface(Activity_Main.SANS);
        ImageView manufacturer_logo = (ImageView) findViewById(R.id.manufacturer_logo);
        Glide
                .with(Activity_Main.CONTEXT)
                .load(Activity_Main.logo)
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(manufacturer_logo);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Directions.checkActivity(Activity_Manufacturer.target);

            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView btnNavigation = (ImageView) findViewById(R.id.btnNavigation);
        btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.RIGHT);
            }
        });
        Fragment squadFragment = new Fragment_Navigation();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.nav_view, squadFragment, null);
        fragmentTransaction.commit();

// Creating adapter that return the fragment
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
// Setup ViewPager with adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(Activity_Products.selectedTab);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mViewPager.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
// Tabs
        MaterialTabs tabs = (MaterialTabs) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setTypefaceSelected(Activity_Main.SANS);
        tabs.setTypefaceUnselected(Activity_Main.SANS);
    }

    // Back Event
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
//@Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        return true;
    }

    private void progressBar() {
        progress.dismiss();

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void addTab(TabLayout.Tab tab) {

    }


    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        String tabTitles[] = new String[]{"اطلاعات تماس", "محصولات"};

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Fragment_manufacturer_data();
                case 1:
                    return new Fragment_manufacturer_products();
            }
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public View getTabView(int position, View v, boolean selected) {
            if (v == null) {
                View tab = LayoutInflater.from(Activity_Manufacturer.this).inflate(R.layout.custom_tab, null);
            }
            tabText = (TextView) v.findViewById(R.id.custom_text);
            tabText.setText(tabTitles[position]);
            tabText.setTypeface(Activity_Main.SANS);
            return v;
        }
    }

    private class Async3 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar();

//            Activity_Main.request = result;
            try {
                JSONObject data = new JSONObject(result);
                JSONObject manufacturer_data = data.getJSONObject("data");
                Fragment_manufacturer_data.data_distribute(manufacturer_data);
                String back_image = manufacturer_data.getString("back_image");
                JSONObject options = manufacturer_data.getJSONObject("options");
                String Stel = options.getString("slogan");
                manufacturer_tell.setText(Stel);
                if (manufacturer_tell.getText().toString().equals("false")) {
                    manufacturer_tell.setVisibility(View.GONE);
                }
                Glide
                        .with(Activity_Main.CONTEXT)
                        .load(back_image)
                        .placeholder(R.drawable.no)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(cover);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(Activity_Manufacturer.this, "مشکل در بارگذاری", Toast.LENGTH_SHORT).show();
//                request_results.setText("هیچ محصولی پیدا نشد!");
            }

        }

    }

    public static void closeDrawer() {
        drawer.closeDrawer(GravityCompat.END);

    }

}
