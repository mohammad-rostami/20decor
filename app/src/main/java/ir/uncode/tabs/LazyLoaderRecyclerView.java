package ir.uncode.tabs;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class LazyLoaderRecyclerView extends RecyclerView {

    private OnLoadListener onLoadListener;
    private static final int ITEM_LOAD_OFFSET = 4;
    private int lastSeenItem = 0;
    private boolean isLoading = false;

    public LazyLoaderRecyclerView(Context context) {
        super(context);
    }

    public LazyLoaderRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LazyLoaderRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnLoadListener(OnLoadListener onLoadListener) {
        this.onLoadListener = onLoadListener;
    }

    public void finishLoading() {
        isLoading = false;
    }

    public void resetValues() {
        isLoading = false;
        lastSeenItem = 0;
    }

    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);

        //Get LinearLayout Manager
        LinearLayoutManager layoutManager = (LinearLayoutManager) this.getLayoutManager();

        //Get Item Details
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
        int lastVisibleItem = layoutManager.findLastVisibleItemPosition();

        //If Last Visible Item Is Bigger Than Or Equal To ITEM_LOAD_OFFSET value To The End
        if (lastVisibleItem >= totalItemCount - ITEM_LOAD_OFFSET) {
            //If Isn't In Loading Process
            if (!isLoading) {
                //If Last Visible Item Shown For First Time (isn't 2nd or 3rd time)
                if (lastVisibleItem > lastSeenItem) {
                    //Change isLoading To True To Prevent Run This Scope When Loading Is On Process
                    isLoading = true;

                    //If Listener Isn't Null, Call That To Load Data
                    if (onLoadListener != null) {
                        onLoadListener.onLoad();
                    }

                    //Save Last Item That User Seen In List
                    lastSeenItem = lastVisibleItem;
                }
            }
        }
    }

    public interface OnLoadListener {
        public void onLoad();
    }
}