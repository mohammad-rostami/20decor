package ir.uncode.tabs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.idunnololz.widgets.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class Activity_ProductsList extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private int Tab = 5;
    private ArrayList<Struct> structs = new ArrayList<>();
    private ArrayList<Struct> structsOrder = new ArrayList<>();
    private JSONArray firstArray;
    private boolean isGrid;
    private LinearLayoutManager manager;
    private RecyclerView.Adapter adapter;
    private ImageView btnSort;
    private static DrawerLayout drawer;
    private TextView request_results;
    private ProgressDialog progress;
    private ProgressDialog progress1;
    public static String name;
    public static String name1 = "";
    private String name2 = "";

    private boolean loading = true;
    LazyLoaderRecyclerView recyclerView;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int page;
    static boolean active = false;
    private Dialog dialog;
    private TextView sort_type;
    private TextView section_label;
    private ExampleAdapter exp_adapter;
    private AnimatedExpandableListView listView;
    private String sort_name;
    private int lastExpandedPosition = -1;
    private Boolean hasChild;
    private JSONArray secondArray;
    private boolean checked = false;
    private boolean checked1 = false;
    private Dialog order_dialog;
    private TextView sort_type_order;
    private JSONArray thirdArray;
    private RecyclerView.Adapter exp_adapter_order;
    private AnimatedExpandableListView listView_order;
    private RecyclerView recyclerViewOrder;
    private LinearLayoutManager managerOrder;
    private String sort_name_order;
    private boolean isLoaded = false;
    private String newFiltertarget;
    private String newFilterCat;
    private String newOrdertarget;
    private String newOrderCat;
    int n = 4;


    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productslist);


        if (G.route != null) {
            G.backUp.add(G.route);
            G.backUp.add(G.dataTarget);
            G.backUp.add(G.dataCode);
        }

        G.route = Activity_Main.target_cat;
        G.dataTarget = G.target;
        G.dataCode = G.target_code;
        Log.d("mohammad", G.route);
        Log.d("mohammad", G.dataTarget);
        Log.d("mohammad", G.dataCode);
        Log.d("mohammad", String.valueOf(G.backUp.size()));
//DEFINE DIALOG BOX
        dialog = new Dialog(Activity_ProductsList.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sort_list);
        dialog.setCancelable(true);

        order_dialog = new Dialog(Activity_ProductsList.this);
        order_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        order_dialog.setContentView(R.layout.order_list);
        order_dialog.setCancelable(true);

// LOADING PROGRESS
//        progress = new ProgressDialog(Activity_ProductsList.this, R.style.MyTheme);
//        progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
//        progress.setCancelable(true);
//        progress.show();

// CONTENT REQUEST
        new Async2().execute(Activity_Main.target_cat, G.dataTarget, G.dataCode, String.valueOf(1));
        request_results = (TextView) findViewById(R.id.request_results);
        request_results.setTypeface(Activity_Main.SANS);
        TextView pageName = (TextView) findViewById(R.id.pageName);
        pageName.setText("لیست محصولات");
        pageName.setTypeface(Activity_Main.SANS);


// GRID / LIST BUTTON
        btnSort = (ImageView) findViewById(R.id.btnSort);
        btnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeRecyclerView(structs, !isGrid);
                if (isGrid) {
                    btnSort.setImageResource(R.drawable.ic_view_grid);
                } else {
                    btnSort.setImageResource(R.drawable.ic_view_list);
                }
            }
        });


// NAVIGATION DRAWER (ON CREATE)
        initializeRecyclerView(structs, false);
        ImageView btnMenu = (ImageView) findViewById(R.id.btnNavigation);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_ProductsList.this, Activity_Products.class);
                Activity_ProductsList.this.startActivity(intent);
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView btnNavigation = (ImageView) findViewById(R.id.btnNavigation);
        btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.RIGHT);
            }
        });
        Fragment squadFragment = new Fragment_Navigation();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.nav_view, squadFragment, null);
        fragmentTransaction.commit();


        //sort toolbar
        LinearLayout sort = (LinearLayout) findViewById(R.id.sort);
        TextView sort_txt = (TextView) findViewById(R.id.sort_txt);
        final ImageView clear_sort = (ImageView) findViewById(R.id.clear_sort);
        sort_type = (TextView) findViewById(R.id.sort_type);
        section_label = (TextView) findViewById(R.id.section_label);
        section_label.setTypeface(Activity_Main.SANS);
        clear_sort.setVisibility(View.GONE);
        sort_txt.setTypeface(Activity_Main.SANS);
        sort_type.setTypeface(Activity_Main.SANS);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
        clear_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                structs.clear();
                adapter.notifyDataSetChanged();
                sort_type.setText("همه محصولات");
                clear_sort.setVisibility(View.GONE);
                checked = false;
                exp_adapter.notifyDataSetChanged();
                name1 = "";
                if (name2 != null && !name2.isEmpty() && !name2.equals("null")) {
                    StringTokenizer tokens = new StringTokenizer(name2, ":");
                    newOrdertarget = "";
                    newOrderCat = "";
                    newOrdertarget = tokens.nextToken();
                    newOrderCat = tokens.nextToken();
                    new Async4().execute("product_list", G.dataTarget, G.dataCode, newOrdertarget, newOrderCat, String.valueOf(1));
                    n = 3;
                } else {
                    new Async2().execute(G.route, G.dataTarget, G.dataCode, String.valueOf(1));
                    n = 4;
                }
            }
        });

        //order toolbar
        LinearLayout sort_order = (LinearLayout) findViewById(R.id.sort_order);
        TextView sort_txt_order = (TextView) findViewById(R.id.sort_txt_order);
        final ImageView clear_sort_order = (ImageView) findViewById(R.id.clear_sort_order);
        sort_type_order = (TextView) findViewById(R.id.sort_type_order);
        clear_sort_order.setVisibility(View.GONE);
        sort_txt_order.setTypeface(Activity_Main.SANS);
        sort_type_order.setTypeface(Activity_Main.SANS);
        sort_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order_dialog.show();
            }
        });
        clear_sort_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                structs.clear();
                adapter.notifyDataSetChanged();
                sort_type_order.setText("همه");
                clear_sort_order.setVisibility(View.GONE);
                Adapter_Recycler.checked = false;
                exp_adapter_order.notifyDataSetChanged();
                name2 = "";
                if (name1 != null && !name1.isEmpty() && !name1.equals("null")) {
                    StringTokenizer tokens = new StringTokenizer(name1, ":");
                    newFiltertarget = "";
                    newFilterCat = "";
                    newFiltertarget = tokens.nextToken();
                    newFilterCat = tokens.nextToken();
                    new Async4().execute("product_list", G.dataTarget, G.dataCode, newFiltertarget, newFilterCat, String.valueOf(1));
                    n = 2;
                } else {
                    new Async2().execute(G.route, G.dataTarget, G.dataCode, String.valueOf(1));
                    n = 4;
                }
            }
        });

        //sort button on dialog box
        TextView change_sort_mode = (TextView) dialog.findViewById(R.id.change_sort_mode);
        change_sort_mode.setTypeface(Activity_Main.SANS);
        change_sort_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    sort_type_order.setText("همه");
//                    clear_sort_order.setVisibility(View.GONE);
                    if (checked == true) {
                        clear_sort.setVisibility(View.VISIBLE);
                        sort_type.setText(sort_name);
                        StringTokenizer tokens = new StringTokenizer(name1, ":");
                        newFiltertarget = "";
                        newFilterCat = "";
                        newFiltertarget = tokens.nextToken();
                        newFilterCat = tokens.nextToken();

                        if (name2 != null && !name2.isEmpty() && !name2.equals("null")) {
                            StringTokenizer token1 = new StringTokenizer(name2, ":");
                            newOrdertarget = "";
                            newOrderCat = "";
                            newOrdertarget = token1.nextToken();
                            newOrderCat = token1.nextToken();
                            new Async4().execute("product_list", G.dataTarget, G.dataCode, newFiltertarget, newFilterCat, newOrdertarget, newOrderCat, String.valueOf(1));
                            n = 1;
                        } else {
                            new Async4().execute("product_list", G.dataTarget, G.dataCode, newFiltertarget, newFilterCat, String.valueOf(1));
                            n = 2;
                        }
                        dialog.dismiss();
                    } else {
                        Toast.makeText(Activity_Main.CONTEXT, "فیلتری انتخاب نشده!", Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(Activity_Main.CONTEXT, "فیلتری انتخاب نشده!", Toast.LENGTH_SHORT).show();
                    sort_type.setText("همه محصولات");
                    clear_sort.setVisibility(View.GONE);
                }
            }
        });
        //order button on dialog box
        TextView change_sort_mode_order = (TextView) order_dialog.findViewById(R.id.change_sort_mode);
        change_sort_mode_order.setTypeface(Activity_Main.SANS);
        change_sort_mode_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Adapter_Recycler.checked == true) {
//                    sort_type.setText("همه محصولات");
//                    clear_sort.setVisibility(View.GONE);

                        clear_sort_order.setVisibility(View.VISIBLE);
                        sort_type_order.setText(sort_name_order);
                        StringTokenizer tokens = new StringTokenizer(name2, ":");
                        newOrdertarget = "";
                        newOrderCat = "";
                        newOrdertarget = tokens.nextToken();
                        newOrderCat = tokens.nextToken();


                        if (name1 != null && !name1.isEmpty() && !name1.equals("null")) {
                            StringTokenizer token1 = new StringTokenizer(name1, ":");
                            newFiltertarget = "";
                            newFilterCat = "";
                            newFiltertarget = token1.nextToken();
                            newFilterCat = token1.nextToken();
                            new Async4().execute("product_list", G.dataTarget, G.dataCode, newOrdertarget, newOrderCat, newFiltertarget, newFilterCat, String.valueOf(1));
                            n = 1;
                        } else {
                            new Async4().execute("product_list", G.dataTarget, G.dataCode, newOrdertarget, newOrderCat, String.valueOf(1));
                            n = 3;
                        }
                        order_dialog.dismiss();
                    } else {
                        Toast.makeText(Activity_Main.CONTEXT, "فیلتری انتخاب نشده!", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(Activity_Main.CONTEXT, "فیلتری انتخاب نشده!", Toast.LENGTH_SHORT).show();
                    sort_type_order.setText("همه");
                    clear_sort_order.setVisibility(View.GONE);
                }
            }
        });

//RECYCLERVIEW
        recyclerViewOrder = (RecyclerView) order_dialog.findViewById(R.id.listViewOrder);
        exp_adapter_order = new Adapter_Recycler(this, structsOrder, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                if (name2 != null && !name2.isEmpty() && !name2.equals("null")) {
                    name2 = "";
                }
                sort_name_order = structsOrder.get(position).strFirst;
                name2 = structsOrder.get(position).strSecond;
            }

            @Override
            public void onItemClick(int position) {

            }
        }, 12, false);

        managerOrder = new LinearLayoutManager(getApplicationContext());
        recyclerViewOrder.setAdapter(exp_adapter_order);
        recyclerViewOrder.setLayoutManager(managerOrder);
        recyclerViewOrder.removeItemDecoration(null);

    }

    // RECYCLER VIEW  (ON CREATE)
    private void initializeRecyclerView(final ArrayList<Struct> structs, boolean isGrid) {
        recyclerView = (LazyLoaderRecyclerView) findViewById(R.id.product_list);
        adapter = new Adapter_Recycler(this, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, isGrid);
        if (isGrid) {
            manager = new LinearLayoutManager(getApplicationContext());
        } else {
            manager = new GridLayoutManager(getApplicationContext(), 2);
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.removeItemDecoration(null);
        recyclerView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        page = 1;
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                customLoadMoreDataFromApi(page);
            }
        });
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0) //check for scroll down
//                {
//                    visibleItemCount = manager.getChildCount();
//                    totalItemCount = manager.getItemCount();
//                    pastVisiblesItems = manager.findFirstVisibleItemPosition();
//
//                    if (loading) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = false;
//                            customLoadMoreDataFromApi(page);
//                            page = page + 1;
//                        }
//                    }
//                }
//            }
//        });


        this.isGrid = isGrid;
    }

    // BACK PRESSED
    @Override
    public void onBackPressed() {
        if (G.backUp.size() > 1) {
            G.route = G.backUp.get(G.backUp.size() - 3);
            G.dataTarget = G.backUp.get(G.backUp.size() - 2);
            G.dataCode = G.backUp.get(G.backUp.size() - 1);
            Log.d("mohammad", G.route);
            Log.d("mohammad", G.dataTarget);
            Log.d("mohammad", G.dataCode);
            G.backUp.remove(G.backUp.size() - 3);
            G.backUp.remove(G.backUp.size() - 2);
            G.backUp.remove(G.backUp.size() - 1);
        }
        name1 = "";
        name2 = "";
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    //@Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        return true;
    }

    // ASYNCK TASK OF MAIN CONTENT
    private class Async2 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(Activity_ProductsList.this, R.style.MyTheme);
            progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress.setCancelable(true);
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar();
            try {
                JSONObject data = new JSONObject(result);
                secondArray = data.getJSONArray("filter_data");
                thirdArray = data.getJSONArray("order_data");
                firstArray = data.getJSONArray("data");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strSecond = secondObject.getString("manufacturer");
                    struct.image = secondObject.getString("thumb");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    String final_price_checker = secondObject.get("special").toString();
                    if (final_price_checker.equals("false")) {
                    } else {
                        struct.strChangedPrice = secondObject.get("special").toString();
                    }
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                section_label.setText("فعلا اینجا هیچی نیست!");
            }
            if (structs.size() == 0) {
                section_label.setText("فعلا اینجا هیچی نیست!");
            } else {
                section_label.setVisibility(View.GONE);
            }

            if (!isLoaded) {
                try {
                    for (int n = 0; n < thirdArray.length(); n++) {
                        JSONObject orderObject = thirdArray.getJSONObject(n);
                        Struct structOrder = new Struct();
                        structOrder.strFirst = orderObject.getString("name");
                        structOrder.strSecond = orderObject.getString("target");
                        structsOrder.add(structOrder);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    request_results.setText("هیچ محصولی پیدا نشد!");
                }
                isLoaded = true;
            }


            //EXPANDABLE LISTVIEW DATA (SORT DIALOG)
            List<GroupItem> items = new ArrayList<GroupItem>();
            try {
                int s = secondArray.length();
                for (int h = 0; h < secondArray.length(); h++) {
                    GroupItem item = new GroupItem();
                    JSONObject groupItem = secondArray.getJSONObject(h);
                    item.title = groupItem.getString("name");
                    item.titleHint = groupItem.getString("target");
//                    JSONObject object_menu_item = Splash_Screen.sort_final_array.getJSONObject(h);
//                    JSONArray object_menu_childs = object_menu_item.getJSONArray("items");
//
//                    for (int j = 0; j < object_menu_childs.length(); j++) {
//                        JSONObject object_menu_child = object_menu_childs.getJSONObject(j);
//                        ChildItem child = new ChildItem();
//                        child.title = object_menu_child.getString("name");
//                        child.hint = object_menu_child.getString("target");
//                        item.items.add(child);
//                    }
                    items.add(item);
                }
                exp_adapter = new ExampleAdapter(getApplicationContext());
                exp_adapter.setData(items);
            } catch (JSONException e) {
                Toast.makeText(Activity_ProductsList.this, "asda", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            exp_adapter.notifyDataSetChanged();
            adapter.notifyDataSetChanged();
            exp_adapter_order.notifyDataSetChanged();


            //EXPANDABLE LISTVIEW (ON CREATE)
            listView = (AnimatedExpandableListView) dialog.findViewById(R.id.listView);
            listView.setAdapter(exp_adapter);
            listView.setDividerHeight(2);
            listView.setGroupIndicator(null);
            listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                    String groupName = exp_adapter.getGroup(groupPosition).titleHint;
                    Activity_Main.groupTitle = exp_adapter.getGroup(groupPosition).title;
//                    StringTokenizer tokens = new StringTokenizer(groupName, ":");
//                    G.target = "";
//                    G.target_code = "";
//                    G.target = tokens.nextToken();
//                    G.target_code = tokens.nextToken();

                    if (listView.isGroupExpanded(groupPosition)) {
                        listView.collapseGroupWithAnimation(groupPosition);
                    } else {
                        listView.expandGroupWithAnimation(groupPosition);

                    }
                    return true;
                }
            });
            listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                @Override
                public void onGroupExpand(int groupPosition) {
                    if (lastExpandedPosition != -1
                            && groupPosition != lastExpandedPosition) {
                        listView.collapseGroup(lastExpandedPosition);
                    }
                    lastExpandedPosition = groupPosition;
                }
            });
            listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                    return true;
                }
            });

        }
    }

    // ASYNC TASK OF PAGINATION
    private class Async3 extends Webservice.PostClassOrder {


        @Override
        protected void onPreExecute() {

            progress1 = new ProgressDialog(Activity_ProductsList.this, R.style.MyTheme);
            progress1.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress1.setCancelable(true);
            progress1.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress1.dismiss();

            try {
                JSONObject data = new JSONObject(result);
//                JSONObject firstObject = data.getJSONObject("data");
                firstArray = data.getJSONArray("data");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strSecond = secondObject.getString("manufacturer");
                    struct.image = secondObject.getString("thumb");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    String final_price_checker = secondObject.get("special").toString();
                    if (final_price_checker.equals("false")) {
                    } else {
                        struct.strChangedPrice = secondObject.get("special").toString();
                    }
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyItemInserted(structs.size() - 1);
            adapter.notifyDataSetChanged();
            recyclerView.finishLoading();
        }
    }

    private void progressBar() {
        progress.dismiss();

    }

    public void customLoadMoreDataFromApi(int page) {
//        Toast.makeText(Activity_ProductsList.this, "sa", Toast.LENGTH_SHORT).show();

        if (n == 1) {
            new Async3().execute("product_list", G.dataTarget, G.dataCode, newFiltertarget, newFilterCat, newOrdertarget, newOrderCat, String.valueOf(page + 1));
//            Toast.makeText(Activity_ProductsList.this, "first", Toast.LENGTH_SHORT).show();
        } else if (n == 3) {
            new Async3().execute("product_list", G.dataTarget, G.dataCode, newOrdertarget, newOrderCat, String.valueOf(page + 1));
//            Toast.makeText(Activity_ProductsList.this, "second", Toast.LENGTH_SHORT).show();

        } else if (n == 2) {
            new Async3().execute("product_list", G.dataTarget, G.dataCode, newFiltertarget, newFilterCat, String.valueOf(page + 1));
//            Toast.makeText(Activity_ProductsList.this, "third", Toast.LENGTH_SHORT).show();

        } else if (name1.equals("") && name2.equals("")) {
            new Async3().execute(G.route, G.dataTarget, G.dataCode, String.valueOf(page + 1));
//            Toast.makeText(Activity_ProductsList.this, "forth", Toast.LENGTH_SHORT).show();

        }
    }


    // ASYNC TASK OF SORT FEATURE
    private class Async4 extends Webservice.PostClassOrder {

        @Override
        protected void onPreExecute() {
            structs.clear();
            adapter.notifyDataSetChanged();
            progress = new ProgressDialog(Activity_ProductsList.this, R.style.MyTheme);
            progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress.setCancelable(true);
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            try {
                JSONObject data = new JSONObject(result);
//                secondArray = data.getJSONArray("filter_data");
                firstArray = data.getJSONArray("data");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strSecond = secondObject.getString("manufacturer");
                    struct.image = secondObject.getString("thumb");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    String final_price_checker = secondObject.get("special").toString();
                    if (final_price_checker.equals("false")) {
                    } else {
                        struct.strChangedPrice = secondObject.get("special").toString();
                    }
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                request_results.setVisibility(View.VISIBLE);
                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();
            Log.d("mohammad", String.valueOf(structs.size()));
            if (structs.size() == 0) {
                section_label.setVisibility(View.VISIBLE);
                section_label.setText("فعلا اینجا هیچی نیست!");
            } else {
                section_label.setVisibility(View.GONE);
            }
        }
    }

    // ASYNC TASK OF SORT FEATURE
    private class Async5 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            structs.clear();
            adapter.notifyDataSetChanged();
            progress = new ProgressDialog(Activity_ProductsList.this, R.style.MyTheme);
            progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress.setCancelable(true);
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            try {
                JSONObject data = new JSONObject(result);
//                secondArray = data.getJSONArray("filter_data");
                firstArray = data.getJSONArray("data");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strSecond = secondObject.getString("manufacturer");
                    struct.image = secondObject.getString("thumb");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    String final_price_checker = secondObject.get("special").toString();
                    if (final_price_checker.equals("false")) {
                    } else {
                        struct.strChangedPrice = secondObject.get("special").toString();
                    }
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();
        }
    }

    // EXPANDABLE LISTVIEW
    private static class GroupItem {
        String title;
        String titleHint;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
    }

    private static class ChildHolder {
        TextView title;
        TextView hint;
    }

    private static class GroupHolder {
        TextView title;
        TextView titleHint;
        RadioButton radio;

    }

    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        int selectedPosition = 0;
        int selectedGroupPosition = 0;

        @Override
        public View getRealChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.expandable_childitem_sort, parent, false);
//                holder.radio = (RadioButton) convertView.findViewById(R.id.radio);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.hint = (TextView) convertView.findViewById(R.id.textHint);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public int getPositionForSection(int section) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.expandable_groupitem_sort_nochild, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.titleHint = (TextView) convertView.findViewById(R.id.textTitleHint);
                holder.radio = (RadioButton) convertView.findViewById(R.id.radio);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }
            if (checked == false) {
                holder.radio.setChecked(false);

            }
            if (checked == true) {
                holder.radio.setChecked(groupPosition == selectedGroupPosition);
            }
            holder.radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checked = true;
                    selectedGroupPosition = groupPosition;
                    notifyDataSetChanged();
                    if (name1 != null && !name1.isEmpty() && !name1.equals("null")) {
                        name = "";
                    }
                    name1 = exp_adapter.getGroup(groupPosition).titleHint;
                    sort_name = exp_adapter.getGroup(groupPosition).title;
                }
            });
            holder.titleHint.setText(item.titleHint);
            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);

            if (getChildrenCount(groupPosition) == 0) {
                hasChild = false;
            } else {
                hasChild = true;
            }

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }
    }

    // DRAWER CLOSE METHOD
    public static void closeDrawer() {
        drawer.closeDrawer(GravityCompat.END);
    }

}
