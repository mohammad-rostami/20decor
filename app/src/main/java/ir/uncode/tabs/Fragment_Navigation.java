package ir.uncode.tabs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.idunnololz.widgets.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class Fragment_Navigation extends Fragment implements Async_Response {
    private AnimatedExpandableListView listView;
    private ExampleAdapter adapter;
    private Boolean hasChild;
    public static String name;


    public Fragment_Navigation() {
        // Required empty public constructor
    }

    @Override
    public void getProcessFinish(String output) {

    }

    @Override
    public void postProcessFinish(String output) {

    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<GroupItem> items = new ArrayList<GroupItem>();
        // Populate our list with groups and it's children

        try {
            for (int h = 0; h < Splash_Screen.GROUP_ITEMS.size(); h++) {
                GroupItem item = new GroupItem();
                item.title = (String) Splash_Screen.GROUP_ITEMS.get(h);
                item.titleHint = (String) Splash_Screen.MENU_TARGETS.get(h);
                JSONObject object_menu_item = Splash_Screen.OBJECT_MENU_ITEMS.getJSONObject(h);
                JSONArray object_menu_childs = object_menu_item.getJSONArray("items");

                for (int j = 0; j < object_menu_childs.length(); j++) {
                    JSONObject object_menu_child = object_menu_childs.getJSONObject(j);
                    ChildItem child = new ChildItem();
                    child.title = object_menu_child.getString("name");
                    child.hint = object_menu_child.getString("target");
                    item.items.add(child);
                }
                items.add(item);
            }
            adapter = new ExampleAdapter(getContext());
            adapter.setData(items);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {

        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_navigation, container, false);
        TextView home_button = (TextView) rootView.findViewById(R.id.home_button);
        home_button.setTypeface(Activity_Main.SANS);
        if (getActivity().getClass().getSimpleName().equals("Activity_Main")) {
            home_button.setVisibility(View.GONE);
        }
        home_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent(G.currentActivity, Activity_Main.class);
                startActivity(view);
            }
        });


        listView = (AnimatedExpandableListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setDividerHeight(2);
        // In order to show animations, we need to use a custom click handler
        // for our ExpandableListView.
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {


            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                String groupName = adapter.getGroup(groupPosition).titleHint;
                Activity_Main.groupTitle = adapter.getGroup(groupPosition).title;
                StringTokenizer tokens = new StringTokenizer(groupName, ":");
                G.target = "";
                G.target_code = "";
                G.target = tokens.nextToken();
                G.target_code = tokens.nextToken();
                Log.d("mohammad", G.target);
                if (G.target.equals("information_info")) {
                    Activity_Main.target_cat = "information_info";
                    if (Activity_ProductsList.active == true) {
                        Activity_ProductsList.closeDrawer();
                    }
                    Activity_Main.closeDrawer();
                    if (Activity_Manufacturer.active == true) {
                        Activity_Manufacturer.closeDrawer();
                    }
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            Intent intent = new Intent(getActivity(), Activity_Information.class);
                            startActivity(intent);
                        }
                    }, 200);
                } else if (G.target.equals("configure")) {
                    if (Activity_ProductsList.active == true) {
                        Activity_ProductsList.closeDrawer();
                    }
                    Activity_Main.closeDrawer();
                    if (Activity_Manufacturer.active == true) {
                        Activity_Manufacturer.closeDrawer();
                    }
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            Intent intent = new Intent(getActivity(), Activity_Setting.class);
                            startActivity(intent);
                        }
                    }, 200);

                } else if (G.target.equals("wishlist")) {
                    if (Activity_ProductsList.active == true) {
                        Activity_ProductsList.closeDrawer();
                    }
                    Activity_Main.closeDrawer();
                    if (Activity_Manufacturer.active == true) {
                        Activity_Manufacturer.closeDrawer();
                    }
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            Intent intent = new Intent(getActivity(), Activity_Wishlist.class);
                            startActivity(intent);
                        }
                    }, 200);

                }

                if (listView.isGroupExpanded(groupPosition)) {
                    listView.collapseGroupWithAnimation(groupPosition);

                } else {
                    listView.expandGroupWithAnimation(groupPosition);

                }
                return true;
            }

        });
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                name = adapter.getChild(groupPosition, childPosition).hint;

                if (Activity_ProductsList.active == true) {
                    Activity_ProductsList.closeDrawer();
                }
                Activity_Main.closeDrawer();
                if (Activity_Manufacturer.active == true) {
                    Activity_Manufacturer.closeDrawer();
                }
                new Handler().postDelayed(new Runnable() {

                    public void run() {
                        Directions.checkActivity(name);
                    }
                }, 200);

//                new Async2().execute("Navigation", G.target, G.target_code, String.valueOf(1));
                return true;
            }
        });
        return rootView;
    }

    private static class GroupItem {
        String title;
        String titleHint;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
    }

    private static class ChildHolder {
        TextView title;
        TextView hint;
    }

    private static class GroupHolder {
        TextView title;
        TextView titleHint;
        ImageView indicator;
    }

    /**
     * Adapter for our list of {@link GroupItem}s.
     */
    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.expandable_childitem, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.hint = (TextView) convertView.findViewById(R.id.textHint);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public int getPositionForSection(int section) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);


            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.expandable_groupitem, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.titleHint = (TextView) convertView.findViewById(R.id.textTitleHint);
                holder.indicator = (ImageView) convertView.findViewById(R.id.indicator);

                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }
            if (getChildrenCount(groupPosition) == 0) {
                holder.indicator.setVisibility(View.INVISIBLE);
            } else {
                holder.indicator.setVisibility(View.VISIBLE);
                holder.indicator.setImageResource(isExpanded ? R.drawable.ic_keyboard_arrow_up_black_24dp : R.drawable.ic_keyboard_arrow_down_black_24dp);
            }
            holder.titleHint.setText(item.titleHint);
            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);

            if (getChildrenCount(groupPosition) == 0) {
                hasChild = false;
            } else {
                hasChild = true;
            }

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }
    }

    private class Async2 extends Webservice.PostClass {


        @Override
        protected void onPreExecute() {
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            Activity_Main.request = result;
            goToAttract();
        }
    }

    public void goToAttract() {
        Intent intent = new Intent(getActivity(), Activity_ProductsList.class);
        startActivity(intent);
    }
}