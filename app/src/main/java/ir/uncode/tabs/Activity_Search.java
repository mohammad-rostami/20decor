package ir.uncode.tabs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_Search extends Activity {
    private int Tab = 10;
    private ArrayList<Struct> structs = new ArrayList<>();
    private JSONArray firstArray;
    private boolean isGrid;
    private RecyclerView.LayoutManager manager;
    private RecyclerView.Adapter adapter;
    private TextView request_results;
    private ProgressDialog progress;
    private ProgressDialog progress1;
    public static String name;
    public String word;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LazyLoaderRecyclerView recyclerView;
    private int page;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Activity_Search.this, Activity_Main.class);
        startActivity(intent);
        overridePendingTransition(R.anim.reverse_slide_in, R.anim.reverse_slide_out);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);



        final EditText search_box = (EditText) findViewById(R.id.search_box);
        search_box.setTypeface(Activity_Main.SANS);
        ImageView back_btn = (ImageView) findViewById(R.id.back_btn);
        ImageView search_btn = (ImageView) findViewById(R.id.search_btn);

// Back Button
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_Search.this, Activity_Main.class);
                startActivity(intent);
                overridePendingTransition(R.anim.reverse_slide_in, R.anim.reverse_slide_out);
                finish();
            }
        });
        search_box.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    structs.clear();
                    adapter.notifyDataSetChanged();

                    word = search_box.getText().toString();
                    Activity_Main.target_cat = "search";
                    new Async2().execute(Activity_Main.target_cat, "search", word, String.valueOf(1));

                    progress = new ProgressDialog(Activity_Search.this, R.style.MyTheme);
                    progress.setCancelable(true);
                    progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
                    progress.show();
                }
                return false;
            }
        });
        search_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                structs.clear();
                adapter.notifyDataSetChanged();

                word = search_box.getText().toString();
                Activity_Main.target_cat = "search";
                new Async2().execute(Activity_Main.target_cat, "search", word, String.valueOf(1));

                progress = new ProgressDialog(Activity_Search.this, R.style.MyTheme);
                progress.setCancelable(true);
                progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
                progress.show();
            }
        });
        initializeRecyclerView(structs, !isGrid);
        request_results = (TextView) findViewById(R.id.request_results);
        request_results.setTypeface(Activity_Main.SANS);

    }

    // RecyclerView
    private void initializeRecyclerView(final ArrayList<Struct> structs, boolean isGrid) {
        recyclerView = (LazyLoaderRecyclerView) findViewById(R.id.product_list);
        adapter = new Adapter_Recycler(this, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, isGrid);
        manager = new GridLayoutManager(getApplicationContext(), 2);

        //Is Grid
        if (isGrid) {
        } else {
            manager = new LinearLayoutManager(getApplicationContext());
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        recyclerView.removeItemDecoration(null);
        recyclerView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
//        page = 1;
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener()
//        {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
//            {
//                if(dy > 0) //check for scroll down
//                {
//                    visibleItemCount = manager.getChildCount();
//                    totalItemCount = manager.getItemCount();
////                    pastVisiblesItems = manager.findFirstVisibleItemPosition();
//
//                    if (loading)
//                    {
//                        if ( (visibleItemCount) >= totalItemCount)
//                        {
//                            loading = false;
//                            //Do pagination.. i.e. fetch new data
//                            customLoadMoreDataFromApi(page);
//                            page = page + 1;
//                        }
//                    }
//                }
//            }
//        });
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                customLoadMoreDataFromApi(page);
            }
        });

        //Save isGrid Value
        this.isGrid = isGrid;
    }

    // Post method Acync
    private class Async2 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            progressBar();

            try {
                JSONObject data = new JSONObject(result);
                JSONArray firstArray = data.getJSONArray("results");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    struct.image = secondObject.getString("image");
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();
            if (structs.size() == 0) {
                request_results.setText("هیچ محصولی پیدا نشد!");
            } else {
                request_results.setVisibility(View.GONE);
            }


        }
    }

    private class Async3 extends Webservice.PostClass {
        @Override
        protected void onPreExecute() {
            progress1 = new ProgressDialog(Activity_Search.this, R.style.MyTheme);
            progress1.setCancelable(false);
            progress1.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress1.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress1.dismiss();

            try {
                JSONObject data = new JSONObject(result);
                JSONArray firstArray = data.getJSONArray("results");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject secondObject = firstArray.getJSONObject(i);
                    Struct struct = new Struct();
                    struct.strFirst = secondObject.getString("name");
                    struct.strThird = secondObject.getString("target");
                    struct.price = secondObject.getString("price");
                    struct.image = secondObject.getString("image");
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                request_results.setText("هیچ محصولی پیدا نشد!");
            }
            adapter.notifyDataSetChanged();

        }
    }

    private void progressBar() {
        progress.dismiss();

    }


    public void customLoadMoreDataFromApi(int page) {
//        Toast.makeText(Activity_Search.this, String.valueOf(page + 1), Toast.LENGTH_SHORT).show();
        new Async3().execute(Activity_Main.target_cat, "search", word, String.valueOf(page + 1));

    }
}

