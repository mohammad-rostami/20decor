package ir.uncode.tabs;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Splash_Screen extends Activity implements Async_Response {
    public static ArrayList GROUP_ITEMS = new ArrayList();
    public static ArrayList IMAGE_URLS = new ArrayList();
    public static ArrayList MENU_TARGETS = new ArrayList();
    public static ArrayList BANNERS = new ArrayList();
    public static ArrayList TARGETS = new ArrayList();
    public static JSONArray OBJECT_MENU_ITEMS;
    public static JSONObject OBJECTS;
    public static String NEW_UPDATE = "no";
    public static String s;
    public static JSONObject routs;
    public static JSONArray cat_array;
    public static ArrayList SORT_NAMES = new ArrayList();
    public static ArrayList SORT_TARGETS = new ArrayList();
    public static JSONArray sort_final_array;
    private String versionCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
// Get app version
        String versionName = null;
        versionCode = null;
        try {
//            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            versionCode = String.valueOf(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        TextView ver = (TextView) findViewById(R.id.ver);
        TextView ver_code = (TextView) findViewById(R.id.ver_code);
//        ver.setText(versionName);
        ver_code.setText(versionCode);

// Execute WebService
        Webservice.PostData asyncTask = new Webservice.PostData();
        asyncTask.postDelegate = this;
        new Async1().execute("http://www.20decor.com/index.php?route=apiv1/home");
        asyncTask.execute("http://www.20decor.com/index.php?route=information/api");
    }

    @Override
    public void getProcessFinish(String output) {

    }

    @Override
    public void postProcessFinish(String output) {
        TextView ver_late = (TextView) findViewById(R.id.ver_late);

// Get update version from API
        try {
            JSONObject object = new JSONObject(output);
            routs = object.getJSONObject("routes");
            JSONObject object1 = object.getJSONObject("version");
            JSONObject object2 = object1.getJSONObject("com.bistdecor.android");
            int version = object2.getInt("min_version");
            int current = Integer.parseInt(versionCode);
            if (version > current) {
                ver_late.setText("new update available");
                NEW_UPDATE = "yes";
            } else {
                ver_late.setText("no update available");
                NEW_UPDATE = "no";
            }

            OBJECT_MENU_ITEMS = object.getJSONArray("menu");
            for (int j = 0; j < OBJECT_MENU_ITEMS.length(); j++) {
                JSONObject object_menu_item = OBJECT_MENU_ITEMS.getJSONObject(j);
                String menu_item = object_menu_item.getString("name");
                String image_url = object_menu_item.getString("image");
                String target = object_menu_item.getString("target");
                GROUP_ITEMS.add(menu_item);
                IMAGE_URLS.add(image_url);
                MENU_TARGETS.add(target);
            }

// Runable thread
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent mainIntent = new Intent(Splash_Screen.this, Activity_Main.class);
                    Splash_Screen.this.startActivity(mainIntent);
                    Splash_Screen.this.finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            }, 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private class Async1 extends Webservice.PostData {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                OBJECTS = new JSONObject(result);
                JSONArray array = OBJECTS.getJSONArray("slider");
                JSONObject final_object = array.getJSONObject(0);
                s = final_object.getString("image");

                for (int i = 1; i < array.length(); i++) {
                    final_object = array.getJSONObject(i);
                    String h = final_object.getString("image");
                    String g = final_object.getString("target");
                    BANNERS.add(h);
                    TARGETS.add(g);
                }

                JSONObject sort_array = OBJECTS.getJSONObject("manufacturer");
                sort_final_array = sort_array.getJSONArray("filter_data");

                for (int f = 0; f < sort_final_array.length(); f++) {
                    JSONObject sort_final_object = sort_final_array.getJSONObject(f);
                    String n = sort_final_object.getString("name");
                    String t = sort_final_object.getString("target");
                    SORT_NAMES.add(n);
                    SORT_TARGETS.add(t);
                }

                cat_array = OBJECTS.getJSONArray("categories");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
