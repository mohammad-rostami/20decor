package ir.uncode.tabs;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class Adapter_Recycler extends RecyclerView.Adapter<Adapter_Recycler.ViewHolder> {
    private OnItemListener onItemListener;
    private Context context;
    private ArrayList<Struct> structs;
    private boolean isGrid;
    private int Tab;
    private Struct selectedGroupPosition;
    public static boolean checked;
    private Boolean x;
    private Boolean m;

    public Adapter_Recycler(Context context, ArrayList<Struct> structs, OnItemListener onItemListener, int Tab, boolean isGrid) {
        this.onItemListener = onItemListener;
        this.context = context;
        this.structs = structs;
        this.isGrid = isGrid;
        this.Tab = Tab;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null;
        if (Tab == 13) {
            view = inflater.inflate(R.layout.item_wishlist, parent, false);
        }
        if (Tab == 12) {
            view = inflater.inflate(R.layout.item_order, parent, false);
        }
        if (Tab == 11) {
            view = inflater.inflate(R.layout.item_productslist, parent, false);
        }
        if (Tab == 10 && isGrid) {
            view = inflater.inflate(R.layout.item_productslist, parent, false);
        }
        if (Tab == 9) {
            view = inflater.inflate(R.layout.item_showcasecategory, parent, false);
        }
        if (Tab == 8) {
            view = inflater.inflate(R.layout.item_imageslider, parent, false);
        }
        if (Tab == 7) {
            view = inflater.inflate(R.layout.item_productsrelated, parent, false);
        }
        if (Tab == 6) {
            view = inflater.inflate(R.layout.item_productscolor, parent, false);
        }
        if (Tab == 5 && isGrid == false) {
            view = inflater.inflate(R.layout.item_productslist, parent, false);
        }
        if (Tab == 5 && isGrid) {
            view = inflater.inflate(R.layout.item_productslist, parent, false);
        }
        if (Tab == 4) {
            view = inflater.inflate(R.layout.item_decor, parent, false);
        }
        if (Tab == 3) {
            view = inflater.inflate(R.layout.item_manufacturers, parent, false);
        }
        if (Tab == 2) {
            view = inflater.inflate(R.layout.item, parent, false);
        }
        if (Tab == 1) {
            view = inflater.inflate(R.layout.item_category, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        x = G.pricePref.getBoolean("price_data", true);
        m = G.producerPref.getBoolean("producer_data", true);

        if (Tab == 13) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtSecond.setText(structs.get(position).strSecond);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtSecond.setTypeface(Activity_Main.SANS);
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .centerCrop()
                    .into(holder.img1);

            holder.remove_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemClick(position);

                    }
                }
            });
        }
        if (Tab == 12) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtSecond.setText(structs.get(position).strSecond);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtSecond.setTypeface(Activity_Main.SANS);
            if (checked == false) {
                holder.radioButton.setChecked(false);
            }
            if (checked == true) {
                holder.radioButton.setChecked(structs.get(position) == selectedGroupPosition);
            }
            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemListener != null) {
                        onItemListener.onItemSelect(position);
                    }
                    checked = true;
                    selectedGroupPosition = structs.get(position);
                    notifyDataSetChanged();
                }
            });

        }
        if (Tab == 11) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtSecond.setText(structs.get(position).strSecond);
            holder.txtSecond.setTypeface(Activity_Main.SANS);
            holder.txtPrice.setText(structs.get(position).strThird);
            holder.txtPrice.setVisibility(View.GONE);
            holder.txtChangedPrice.setVisibility(View.GONE);
            holder.txtChangedPrice.setTypeface(Activity_Main.SANS);
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgBanner);

            if (x) {
                holder.txtSecond.setVisibility(View.VISIBLE);
            } else if (!x) {
                holder.txtSecond.setVisibility(View.GONE);
            }
        }
        if (Tab == 10) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtSecond.setText(structs.get(position).strSecond);
            holder.txtSecond.setVisibility(View.GONE);
            holder.txtPrice.setText(structs.get(position).price);
            holder.txtPrice.setTypeface(Activity_Main.SANS);
            holder.txtChangedPrice.setVisibility(View.GONE);

            if (x) {
                holder.txtPrice.setVisibility(View.VISIBLE);
            } else if (!x) {
                holder.txtPrice.setVisibility(View.GONE);
            }
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .into(holder.imgBanner);
        }
        if (Tab == 9) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemSelect(position);
                    }
                }
            });
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imgBanner);
        }
        if (Tab == 8) {
            holder.txtFirst.setVisibility(View.GONE);
            holder.txtSecond.setVisibility(View.GONE);
            holder.txtPrice.setVisibility(View.GONE);
            holder.LlPriceCancel.setVisibility(View.GONE);
            holder.txtChangedPrice.setVisibility(View.GONE);
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .fitCenter()
                    .into(holder.imgBanner);
        }
        if (Tab == 7) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtPrice.setText(structs.get(position).strSecond);
            holder.txtSecond.setVisibility(View.GONE);
            holder.LlPriceCancel.setVisibility(View.GONE);
            holder.txtChangedPrice.setVisibility(View.GONE);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtPrice.setTypeface(Activity_Main.SANS);
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .into(holder.imgBanner);

            if (x) {
                holder.txtPrice.setVisibility(View.VISIBLE);
            } else if (!x) {
                holder.txtPrice.setVisibility(View.GONE);
            }
        }
        if (Tab == 6) {

        }
        if (Tab == 5) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtSecond.setText(structs.get(position).strSecond);
            holder.txtPrice.setText(structs.get(position).price);
            holder.txtChangedPrice.setText(structs.get(position).strChangedPrice);
            if (holder.txtChangedPrice.getText().toString() == "") {
                holder.LlPriceCancel.setVisibility(View.GONE);
                holder.txtChangedPrice.setVisibility(View.GONE);
            } else {
                holder.LlPriceCancel.setVisibility(View.VISIBLE);
                holder.txtChangedPrice.setVisibility(View.VISIBLE);
            }
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtSecond.setTypeface(Activity_Main.SANS);
            holder.txtPrice.setTypeface(Activity_Main.SANS);
            holder.txtChangedPrice.setTypeface(Activity_Main.SANS);
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .into(holder.imgBanner);
            if (x) {
                holder.txtPrice.setVisibility(View.VISIBLE);
            } else if (!x) {
                holder.txtPrice.setVisibility(View.GONE);
            }
            if (m) {
                holder.txtSecond.setVisibility(View.VISIBLE);
            } else if (!m) {
                holder.txtSecond.setVisibility(View.GONE);
            }

        }

        if (Tab == 4) {
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemSelect(position);
                    }
                }
            });
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .fitCenter()
                    .into(holder.imgBanner);
        }

        if (Tab == 3) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtSecond.setText(structs.get(position).strSecond);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.txtSecond.setTypeface(Activity_Main.SANS);
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemSelect(position);
                    }
                }
            });
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .placeholder(R.drawable.no)
                    .fitCenter()
                    .into(holder.imgBanner);
        }
        if (Tab == 2) {
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemSelect(position);
                    }
                }
            });
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .fitCenter()
                    .into(holder.imgBanner);
        }

        if (Tab == 1) {
            holder.txtFirst.setText(structs.get(position).strFirst);
            holder.txtFirst.setTypeface(Activity_Main.SANS);
            holder.itemLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemListener != null) {
                        onItemListener.onItemSelect(position);
                    }
                }
            });
            Glide
                    .with(Activity_Main.CONTEXT)
                    .load(structs.get(position).image)
                    .fitCenter()
                    .into(holder.imgBanner);
        }
        holder.txtThird.setText(structs.get(position).strThird);
        holder.txtThird.setTypeface(Activity_Main.SANS);
        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemListener != null) {
                    onItemListener.onItemSelect(position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return structs.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView itemLayout;
        ImageView imgBanner;
        ImageView img1;
        ImageView remove_favorite;
        TextView txtFirst;
        TextView txtSecond;
        TextView txtThird;
        TextView txtPrice;
        TextView txtChangedPrice;
        LinearLayout LlPriceCancel;
        RadioButton radioButton;

        public ViewHolder(View itemView) {
            super(itemView);
            itemLayout = (CardView) itemView.findViewById(R.id.item_layout);
            imgBanner = (ImageView) itemView.findViewById(R.id.img_banner);
            img1 = (ImageView) itemView.findViewById(R.id.img1);
            remove_favorite = (ImageView) itemView.findViewById(R.id.remove_favorite);
            txtFirst = (TextView) itemView.findViewById(R.id.txt_first);
            txtSecond = (TextView) itemView.findViewById(R.id.txt_second);
            txtThird = (TextView) itemView.findViewById(R.id.txt_third);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            txtChangedPrice = (TextView) itemView.findViewById(R.id.txt_changedPrice);
            LlPriceCancel = (LinearLayout) itemView.findViewById(R.id.ll_priceCancel);
            radioButton = (RadioButton) itemView.findViewById(R.id.radiobtn);

        }
    }

}
