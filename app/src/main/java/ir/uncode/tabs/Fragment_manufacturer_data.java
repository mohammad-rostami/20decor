package ir.uncode.tabs;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Fragment_manufacturer_data extends Fragment {
    public ArrayList<Struct> structs = new ArrayList<>();
    private int Tab = 4;
    public static String name;
    private ProgressDialog progress;
    private String target;
    private String target_code;
    private static TextView tel;
    private TextView tel_object;
    private static TextView tel_mob;
    private TextView tel_mob_object;
    private static TextView tel_fax;
    private TextView tel_fax_object;
    private static TextView address;
    private TextView address_object;
    private static TextView website;
    private TextView website_object;
    static LinearLayout phone_layout;
    static LinearLayout fax_layout;
    static LinearLayout web_layout;
    static LinearLayout place_layout;

    public Fragment_manufacturer_data() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);

        void addTab(TabLayout.Tab tab);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        try {
//            JSONObject manufacturer = Splash_Screen.OBJECTS.getJSONObject("look");
//            JSONArray data = manufacturer.getJSONArray("data");
//
//            for (int i = 0; i < data.length(); i++) {
//                Struct struct = new Struct();
//                JSONObject items = data.getJSONObject(i);
//                struct.strFirst = "nothing";
//                struct.strSecond = "nothing";
//                struct.image = items.getString("image");
//                struct.strThird = items.getString("target");
//                structs.add(struct);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_manufacturer_data, container, false);
        tel = (TextView) rootView.findViewById(R.id.tel);
        tel_object = (TextView) rootView.findViewById(R.id.tel_object);
        tel_mob = (TextView) rootView.findViewById(R.id.tel_mob);
        tel_mob_object = (TextView) rootView.findViewById(R.id.tel_mob_object);
        tel_fax = (TextView) rootView.findViewById(R.id.tel_fax);
        tel_fax_object = (TextView) rootView.findViewById(R.id.tel_fax_object);
        address = (TextView) rootView.findViewById(R.id.address);
        address_object = (TextView) rootView.findViewById(R.id.address_object);
        website = (TextView) rootView.findViewById(R.id.website);
        website_object = (TextView) rootView.findViewById(R.id.website_object);

        phone_layout = (LinearLayout) rootView.findViewById(R.id.phone_layout);
        fax_layout = (LinearLayout) rootView.findViewById(R.id.fax_layout);
        web_layout = (LinearLayout) rootView.findViewById(R.id.web_layout);
        place_layout = (LinearLayout) rootView.findViewById(R.id.place_layout);


        tel.setTypeface(Activity_Main.SANS);
        tel_object.setTypeface(Activity_Main.SANS);
        tel_mob.setTypeface(Activity_Main.SANS);
        tel_mob_object.setTypeface(Activity_Main.SANS);
        tel_fax.setTypeface(Activity_Main.SANS);
        tel_fax_object.setTypeface(Activity_Main.SANS);
        address.setTypeface(Activity_Main.SANS);
        address_object.setTypeface(Activity_Main.SANS);
        website.setTypeface(Activity_Main.SANS);
        website_object.setTypeface(Activity_Main.SANS);
        return rootView;
    }

    public static void data_distribute(JSONObject manufacturer_data) {
        try {
            String Sname = manufacturer_data.getString("name");
            String Stel = manufacturer_data.getString("tel");
            String Smob_tel = manufacturer_data.getString("mob_tel");
            JSONObject options = manufacturer_data.getJSONObject("options");
            String Sfax = options.getString("fax");
            String Swebsite = options.getString("website");
            String Saddress = options.getString("address");
            tel.setText(Stel);
            tel_mob.setText(Smob_tel);
            tel_fax.setText(Sfax);
            website.setText(Swebsite);
            address.setText(Saddress);
            if (tel.getText().toString().equals("false")) {
                phone_layout.setVisibility(View.GONE);
            }
            if (tel_fax.getText().toString().equals("false")) {
                fax_layout.setVisibility(View.GONE);
            }
            if (website.getText().toString().equals("false")) {
                web_layout.setVisibility(View.GONE);
            }
            if (address.getText().toString().equals("false")) {
                place_layout.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void progressBar() {
        progress.dismiss();

    }


}