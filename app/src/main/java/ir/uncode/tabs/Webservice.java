package ir.uncode.tabs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;


public class Webservice {
    EditText txt;
    TextView part1;
    TextView part2;
    String data;
    String[] separated;
    private ProgressDialog progress;

    static class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String Url = urls[0];
            HttpURLConnection urlConnection = null;
            String result = "";
            try {
                URL url = new URL(Url);
                urlConnection = (HttpURLConnection) url.openConnection();

                int code = urlConnection.getResponseCode();

                if (code == 200) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    if (in != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                        String line = "";

                        while ((line = bufferedReader.readLine()) != null)
                            result += line;
                    }
                    in.close();
                }

                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return result;
        }
    }


    static class PostData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String Url = urls[0];

            URL object = null;
            String result = null;
            try {
                object = new URL(Url);

                result = "";

                HttpURLConnection con = (HttpURLConnection) object.openConnection();
                con.setDoOutput(true);
                con.setDoInput(true);
                con.setRequestProperty("Content-Type", "application/json");
                con.setRequestProperty("Accept", "application/json");
                con.setRequestMethod("POST");

                JSONObject cred = new JSONObject();
                JSONObject auth = new JSONObject();
                JSONObject parent = new JSONObject();

                cred.put("username", "adm");
                cred.put("password", "pwd");

                auth.put("tenantName", "adm");
                auth.put("passwordCredentials", cred.toString());

                parent.put("auth", auth.toString());

                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                wr.write(parent.toString());
                wr.flush();

                StringBuilder sb = new StringBuilder();
                int HttpResult = con.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(con.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                        result += line;
                    }
                    br.close();
                    System.out.println("" + sb.toString());
                } else {
                    System.out.println(con.getResponseMessage());
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ;
            return result;
        }

        public Splash_Screen postDelegate = null;

        @Override
        protected void onPostExecute(String result) {
            postDelegate.postProcessFinish(result);
        }
    }

    static class PostClass extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String linkType = urls[0];
            String firstPart = urls[1];
            String secondPart = urls[2];
            String thirdPart = urls[3];
            String Url = null;
            try {
                if (firstPart.equals("notif")) {
                    Url = "http://www.20decor.com/index.php?route=apiv1/notification";
                } else if (firstPart != "notif") {
                    Url = Splash_Screen.routs.getString(linkType);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Integer thirdConvert = Integer.parseInt(thirdPart);
            String post_result = null;

            try {
                URL url = new URL(Url);
                post_result = "";
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                Map<String, Object> params1 = new LinkedHashMap<>();
                params1.put("page", thirdConvert);
                params1.put(firstPart, secondPart);

                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params1.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                String urlParameters = postData.toString();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                connection.setDoOutput(true);
//                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
//                dStream.writeBytes(urlParameters.toString());
//                dStream.flush();
//                dStream.close();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                bw.write(urlParameters.toString());
                bw.flush();
                bw.close();
                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters.toString());
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters.toString());
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "POST");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                    post_result += line;
                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return post_result;
        }


    }

    static class PostClassOrder extends AsyncTask<String, Void, String> {

        private String s;
        private String d;

        @Override
        protected String doInBackground(String... urls) {
            int x = urls.length;

            String linkType = urls[0];
//            String firstPart = urls[1];
//            String secondPart = urls[2];
//            String thirdPart = urls[3];
//            String forthPart = urls[4];
//            String fifthPart = urls[5];
            String Url = null;
            try {
//                if (firstPart.equals("notif")) {
//                    Url = "http://www.20decor.com/index.php?route=apiv1/notification";
//                } else if (firstPart != "notif") {
                Url = Splash_Screen.routs.getString(linkType);
//                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            String l = urls[urls.length - 1];
            Integer thirdConvert = Integer.parseInt(l);
            String post_result = null;

            Map<String, Object> params1 = new LinkedHashMap<>();
            params1.put("page", thirdConvert);
            for (int n = 1; n < (urls.length - 1); n += 2) {
                s = "";
                s = urls[n];
                d = "";
                d = urls[n + 1];
                params1.put(s, d);
                Log.w("mohammad", s + d);
            }

            try {
                URL url = new URL(Url);
                post_result = "";
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();


                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params1.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                String urlParameters = postData.toString();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                connection.setDoOutput(true);
//                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
//                dStream.writeBytes(urlParameters.toString());
//                dStream.flush();
//                dStream.close();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
                bw.write(urlParameters.toString());
                bw.flush();
                bw.close();
                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters.toString());
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters.toString());
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "POST");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                    post_result += line;
                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());


            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return post_result;
        }


    }


    static class PostClassNoCat extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String linkType = urls[0];
            String Url = null;
            try {
                Url = Splash_Screen.routs.getString(linkType);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String thirdPart = urls[1];
            Integer thirdConvert = Integer.parseInt(thirdPart);
            String post_result = null;
            try {
                URL url = new URL(Url);
                post_result = "";
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                Map<String, Object> params1 = new LinkedHashMap<>();
                params1.put("page", thirdConvert);
                StringBuilder postData = new StringBuilder();
                for (Map.Entry<String, Object> param : params1.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                    postData.append('=');
                    postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                }
                String urlParameters = postData.toString();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("USER-AGENT", "Mozilla/5.0");
                connection.setRequestProperty("ACCEPT-LANGUAGE", "en-US,en;0.5");
                connection.setDoOutput(true);
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParameters.toString());
                dStream.flush();
                dStream.close();
                int responseCode = connection.getResponseCode();

                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Post parameters : " + urlParameters.toString());
                System.out.println("Response Code : " + responseCode);

                final StringBuilder output = new StringBuilder("Request URL " + url);
                output.append(System.getProperty("line.separator") + "Request Parameters " + urlParameters.toString());
                output.append(System.getProperty("line.separator") + "Response Code " + responseCode);
                output.append(System.getProperty("line.separator") + "Type " + "POST");
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                StringBuilder responseOutput = new StringBuilder();
                System.out.println("output===============" + br);
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                    post_result += line;
                }
                br.close();

                output.append(System.getProperty("line.separator") + "Response " + System.getProperty("line.separator") + System.getProperty("line.separator") + responseOutput.toString());
            } catch (MalformedURLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return post_result;
        }

    }

}
