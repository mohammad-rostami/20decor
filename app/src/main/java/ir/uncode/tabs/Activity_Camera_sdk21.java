package ir.uncode.tabs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Random;

public class Activity_Camera_sdk21 extends Activity implements View.OnTouchListener {

    private ImageView background;
    private ImageView share;
    private ImageView size;
    private static RelativeLayout ll;
    private static File file;

    private Activity_Camera_sdk21_SurfaceView maPreview;
    private FrameLayout maLayoutPreview;
    private Camera maCamera;
    private FrameLayout preview;
    private LinearLayout transparent_layout;

    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    Matrix savedMatrix2 = new Matrix();
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    private float MAX_ZOOM;
    private float[] lastEvent;
    private float d;
    private float newRot;
    private ImageView capture;

    private Boolean imageTaken;
    private Boolean capturePause = false;
    private Boolean isStopped = false;

    private Bitmap bmp;
    private ImageView img;
    private ProgressDialog progress;

    @Override
    protected void onStop() {
        super.onStop();
        if (capturePause == true) {
            isStopped = true;

        } else {
            if (maPreview != null) {
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                preview.removeView(maPreview);
                maPreview = null;
                Toast.makeText(Activity_Camera_sdk21.this, "stop", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (capturePause == false) {
            maCamera = getCameraInstance(0);    //Open rear camer
            maPreview = new Activity_Camera_sdk21_SurfaceView(this, maCamera);
            preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(maPreview);
            Toast.makeText(Activity_Camera_sdk21.this, "restart", Toast.LENGTH_SHORT).show();
        }
        if (capturePause == true) {

//            preview.addView(maPreview);
            Toast.makeText(Activity_Camera_sdk21.this, "restart", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cam);
        imageTaken = false;
        final String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

        capture = (ImageView) findViewById(R.id.capture);
        img = (ImageView) findViewById(R.id.img_product);
        share = (ImageView) findViewById(R.id.share);
        size = (ImageView) findViewById(R.id.x);
        share.setVisibility(View.GONE);
        size.setVisibility(View.GONE);
        background = (ImageView) findViewById(R.id.background);
        ll = (RelativeLayout) findViewById(R.id.main_layout);
        transparent_layout = (LinearLayout) findViewById(R.id.transparent_layer);
        Display display = getWindowManager().getDefaultDisplay();
        SCREEN_WIDTH = getWindowManager().getDefaultDisplay().getWidth();
        SCREEN_HEIGHT = getWindowManager().getDefaultDisplay().getHeight();
        share.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button_disabled));


        progress = new ProgressDialog(Activity_Camera_sdk21.this, R.style.MyTheme);
        progress.setCancelable(false);
        progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
        progress.show();
        Glide
                .with(Activity_Main.CONTEXT)
                .load(Activity_Products.env_image)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progress.dismiss();
                        return false;
                    }
                })
                .into(img);
        img.setOnTouchListener(this);

//        Matrix matrix = new Matrix();
//        img.setScaleType(ImageView.ScaleType.MATRIX);
//        matrix.postRotate((float) 90, img.getDrawable().getBounds().width()/2, img.getDrawable().getBounds().height()/2);
//        img.setImageMatrix(matrix);

        maCamera = getCameraInstance(0);
        maLayoutPreview = (FrameLayout) findViewById(R.id.camera_preview);
        maPreview = new Activity_Camera_sdk21_SurfaceView(this, maCamera);

        maCamera.setDisplayOrientation(90);
        Camera.Parameters params = maCamera.getParameters();
        maLayoutPreview.getLayoutParams().width = params.getPreviewSize().height;
        maLayoutPreview.getLayoutParams().height = params.getPreviewSize().width;

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageTaken == false) {
                    Toast.makeText(Activity_Camera_sdk21.this, "اپتدا تصویر را دخیره کنید", Toast.LENGTH_SHORT).show();
                } else if (imageTaken == true) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("image/png");
                    String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), bmp, "title", null);
                    Uri bmpUri = Uri.parse(pathofBmp);
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            }
        });
        capture.setOnClickListener(new View.OnClickListener() {
            public int PERMISSION_ALL;

            @Override
            public void onClick(View v) {
                if (share.getVisibility() == View.GONE) {
                    capturePause = true;
                    capture.setImageDrawable(getResources().getDrawable(R.drawable.ic_sd_card_black_24dp));
                    maCamera.takePicture(null, null, Activity_Camera_sdk21_SurfaceView.mPicture);//TAKING THE PICTURE
                    share.setVisibility(View.VISIBLE);
                    size.setVisibility(View.VISIBLE);
                } else if (share.getVisibility() == View.VISIBLE) {
                    if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(Activity_Camera_sdk21.this, PERMISSIONS, PERMISSION_ALL);
                        Toast.makeText(Activity_Camera_sdk21.this, "دسترسی های لازم برای استفاده از دوربین صادر نشده است", Toast.LENGTH_LONG).show();
                    }
                    if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
                        if (imageTaken == true) {

                        } else {
                            background.setImageBitmap(Activity_Camera_sdk21_SurfaceView.bmp1);
                            capture.setVisibility(View.GONE);
                            share.setVisibility(View.GONE);
                            size.setVisibility(View.GONE);
                            transparent_layout.setVisibility(View.GONE);
                            Toast.makeText(Activity_Camera_sdk21.this, "تصویر ذخیره شد", Toast.LENGTH_SHORT).show();
                            Random num = new Random();
                            ll.setDrawingCacheEnabled(true); //CamView OR THE NAME OF YOUR LAYOUR
                            ll.buildDrawingCache(true);
                            bmp = Bitmap.createBitmap(ll.getDrawingCache());
                            ll.setDrawingCacheEnabled(false); // clear drawing cache
                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root + "/req_images");
                            myDir.mkdirs();
                            Random generator = new Random();
                            int n = 10000;
                            n = generator.nextInt(n);
                            String fname = "Image-" + n + ".jpg";
                            file = new File(myDir, fname);
//                    Log.i(TAG, "" + file);
                            if (file.exists())
                                file.delete();
                            try {
                                FileOutputStream out = new FileOutputStream(file);
                                bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                out.flush();
                                out.close();
                                background.setImageBitmap(null);
                                Activity_Camera_sdk21_SurfaceView.bmp1.recycle();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            capture.setVisibility(View.VISIBLE);
                            share.setVisibility(View.VISIBLE);
                            size.setVisibility(View.VISIBLE);
                            transparent_layout.setVisibility(View.VISIBLE);
                            capture.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button_disabled));
                            share.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button));
                            imageTaken = true;
                        }
                    }
                }

            }
        });

        Point displayDim = getDisplayWH();
        Point layoutPreviewDim = calcCamPrevDimensions(displayDim,
                maPreview.getOptimalPreviewSize(maPreview.mSupportedPreviewSizes,
                        displayDim.x, displayDim.y));
        if (layoutPreviewDim != null) {
            RelativeLayout.LayoutParams layoutPreviewParams =
                    (RelativeLayout.LayoutParams) maLayoutPreview.getLayoutParams();
            layoutPreviewParams.width = layoutPreviewDim.x;
            layoutPreviewParams.height = layoutPreviewDim.y;
            layoutPreviewParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            maLayoutPreview.setLayoutParams(layoutPreviewParams);
        }
        maLayoutPreview.addView(maPreview);
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private Point getDisplayWH() {

        Display display = this.getWindowManager().getDefaultDisplay();
        Point displayWH = new Point();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(displayWH);
            return displayWH;
        }
        displayWH.set(display.getWidth(), display.getHeight());
        return displayWH;
    }

    private Point calcCamPrevDimensions(Point disDim, Camera.Size camDim) {

        Point displayDim = disDim;
        Camera.Size cameraDim = camDim;

        double widthRatio = (double) displayDim.x / cameraDim.width;
        double heightRatio = (double) displayDim.y / cameraDim.height;

        // use ">" to zoom preview full screen
        if (widthRatio < heightRatio) {
            Point calcDimensions = new Point();
            calcDimensions.x = displayDim.x;
            calcDimensions.y = (displayDim.x * cameraDim.height) / cameraDim.width;
            return calcDimensions;
        }
        // use "<" to zoom preview full screen
        if (widthRatio > heightRatio) {
            Point calcDimensions = new Point();
            calcDimensions.x = (displayDim.y * cameraDim.width) / cameraDim.height;
            calcDimensions.y = displayDim.y;
            return calcDimensions;
        }
        return null;
    }

    public static Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
            Camera.Parameters cp = c.getParameters();
        } catch (Exception e) {
        }
        return c; // returns null if camera is unavailable
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        ImageView view = (ImageView) v;
        view.setScaleType(ImageView.ScaleType.MATRIX);

        float scale;

        // Dump touch event to log
        dumpEvent(event);

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: //first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                lastEvent = new float[4];
                lastEvent[0] = event.getX(0);
                lastEvent[1] = event.getX(1);
                lastEvent[2] = event.getY(0);
                lastEvent[3] = event.getY(1);
                d = rotation(event);
                break;

            case MotionEvent.ACTION_UP: //first finger lifted
            case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                mode = NONE;
                break;


            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    // ...
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x, event.getY()
                            - start.y);
                } else if (mode == ZOOM && event.getPointerCount() == 2) {
                    float newDist = spacing(event);
                    matrix.set(savedMatrix);
                    if (newDist > 10f) {
                        scale = newDist / oldDist;
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                    if (lastEvent != null) {
                        newRot = rotation(event);
                        float r = newRot - d;
                        matrix.postRotate(r, view.getMeasuredWidth() / 2,
                                view.getMeasuredHeight() / 2);
                    }
                }
                break;

        }
        // Perform the transformation
        view.setImageMatrix(matrix);

        return true; // indicate event was handled

    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);

        return (float) Math.toDegrees(radians);
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }


    /**
     * Show an event in the LogCat view, for debugging
     */

    private void dumpEvent(MotionEvent event) {
        String names[] = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }

        sb.append("[");

        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())

                sb.append(";");
        }

        sb.append("]");

    }

    @Override
    public void onBackPressed() {
        imageTaken = false;

        if (share.getVisibility() == View.GONE) {
            finish();
        } else if (share.getVisibility() == View.VISIBLE) {
            capturePause = false;
            capture.setImageDrawable(getResources().getDrawable(R.drawable.ic_photo_camera_black_24dp));
            capture.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button));
            share.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button_disabled));
            if (isStopped == true) {
                FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
                preview.removeView(maPreview);
                maPreview = null;
                maCamera = getCameraInstance(0);    //Open rear camer
                maPreview = new Activity_Camera_sdk21_SurfaceView(this, maCamera);
                preview = (FrameLayout) findViewById(R.id.camera_preview);
                preview.addView(maPreview);
                isStopped = false;
            }
            Activity_Camera_sdk21_SurfaceView.mCamera.startPreview();

            Activity_Camera_sdk21_SurfaceView.bmp1.recycle();
            Activity_Camera_sdk21_SurfaceView.bmp1 = null;
            background.setImageBitmap(null);
            share.setVisibility(View.GONE);
            size.setVisibility(View.GONE);
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}