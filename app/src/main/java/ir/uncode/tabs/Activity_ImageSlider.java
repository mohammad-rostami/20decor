package ir.uncode.tabs;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.idunnololz.widgets.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;


public class Activity_ImageSlider extends FragmentActivity {
    private int Tab = 6;
    private ArrayList<Struct> structs = new ArrayList<>();
    private ArrayList<Struct> relatedStructs = new ArrayList<>();
    private JSONArray firstArray;
    private boolean isGrid;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static ArrayList mImageSlider = new ArrayList();
    private AnimatedExpandableListView listView;
    private JSONArray attribArray;
    private JSONArray other_products;
    private JSONObject firstObject;
    private static PhotoViewAttacher mAttacher;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mImageSlider.clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageslider);

        ImageView btnBack = (ImageView) findViewById(R.id.btnBack);
        ImageView btn_in_room = (ImageView) findViewById(R.id.btn_in_room);
        ImageView btn_like = (ImageView) findViewById(R.id.btn_like);
        btn_in_room.setVisibility(View.GONE);
        btn_like.setVisibility(View.GONE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                mImageSlider.clear();

            }
        });
// Page Contents
        try {
            JSONObject data = new JSONObject(Activity_Main.request);
            firstObject = data.getJSONObject("data");
            JSONObject secondObject = firstObject.getJSONObject("manufacturer_info");
            attribArray = firstObject.getJSONArray("attribute_groups");

            firstArray = firstObject.getJSONArray("images");
            for (int i = 0; i < firstArray.length(); i++) {
                Struct struct = new Struct();
                JSONObject items = firstArray.getJSONObject(i);
                struct.image = items.getString("popup");
                relatedStructs.add(struct);
                String imageSlider = items.getString("popup");
                mImageSlider.add(imageSlider);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

// view pager (onCreate)

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
//        mViewPager.setCurrentItem(mViewPager.getAdapter().getCount() - 1, false);

// Related Items (recyclerView)

        final RecyclerView recyclerViewRelated = (RecyclerView) findViewById(R.id.related);
//        try {
//            other_products = firstObject.getJSONArray("related_products");
//            String relatedChecker = firstObject.get("related_products").toString();
//
//            for (int l = 0; l < mImageSlider.size(); l++) {
//                Struct struct = new Struct();
//                struct.image = String.valueOf(mImageSlider.get(l));
//                relatedStructs.add(struct);
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//
//        }

        RecyclerView.Adapter adapter_related = new Adapter_Recycler(this, relatedStructs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                String name = relatedStructs.get(position).strThird;
                mViewPager.setCurrentItem(position);
//                Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
                mAttacher.update();

            }

            @Override
            public void onItemClick(int position) {

            }
        }, 8, false);

        RecyclerView.LayoutManager manager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewRelated.setAdapter(adapter_related);
        recyclerViewRelated.setLayoutManager(manager1);
        recyclerViewRelated.removeItemDecoration(null);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
                mAttacher.update();
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                mAttacher.update();

            }

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                recyclerViewRelated.smoothScrollToPosition(position);
                mAttacher.update();
            }
        });
    }

    // View Pager
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_slider, container, false);
            final TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            final ImageView section_image = (ImageView) rootView.findViewById(R.id.section_image);
            section_image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            mAttacher = new PhotoViewAttacher(section_image);
            mAttacher.update();

            Glide
                    .with(getContext())
                    .load(mImageSlider.get(getArguments().getInt(ARG_SECTION_NUMBER) - 1))
                    .placeholder(R.drawable.background_card)
                    .fitCenter()
                    .into(section_image);
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // number of pages
            return mImageSlider.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
