package ir.uncode.tabs;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.idunnololz.widgets.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;


public class Activity_Products extends FragmentActivity {
    private int Tab = 6;
    private ArrayList<Struct> structs = new ArrayList<>();
    private ArrayList<Struct> relatedStructs = new ArrayList<>();
    private JSONArray firstArray;
    private boolean isGrid;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static ArrayList mImageSlider = new ArrayList();
    private AnimatedExpandableListView listView;
    private ExampleAdapter adapterExp;
    private JSONArray attribArray;
    private JSONArray other_products;
    public static JSONObject firstObject;
    Integer myNum = 1;
    WebView desc;
    LinearLayout desc_list;
    TextView description;
    LinearLayout attrib_list;
    TextView attribute;
    TextView txt_model;
    TextView relate;
    TextView care;
    TextView no_related;
    TextView product_color;
    TextView txt_manufacturer;
    TextView txt_tel;
    TextView txt_price;
    TextView txt_mobile;
    TextView txt_no_tel;
    TextView txt1;
    TextView txt2;
    TextView txt3;
    ImageView call;
    ImageView compare;
    ImageView btnBack;
    ImageView btn_in_room;
    ImageView btn_share;
    List<GroupItem> items;
    RecyclerView.Adapter adapter_related;
    public static String name;
    private ProgressDialog progress;
    private LinearLayout color_layout;
    private LinearLayout price_layout;
    private LinearLayout relativeTopic;
    private RelativeLayout relativeLayout;
    private Intent intent;
    private int PERMISSION_ALL;
    public static String env_image;
    private String manufacturerTarget;
    public static int selectedTab;
    private String manufacturerName;
    private String manufacturerLogo;
    private String shareContent;
    private boolean x;
    private boolean m;
    private LinearLayout main_ll;
    private ImageView btn_like;
    private SharedPreferences.Editor favPrefEdit;
    private SharedPreferences.Editor favImagePrefEdit;
    private Set<String> set;
    private String productName;
    Boolean liked = false;
    private List<String> sample;
    private SharedPreferences.Editor favNamePrefEdit;
    private Set<String> setName;
    private List<String> sampleName;
    private String productTarget;
    private SharedPreferences.Editor favPicPrefEdit;
    private Set<String> setPic;
    private List<String> samplePic;
    private String tomb;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mImageSlider.clear();
        if (G.isWishlist == true) {
            Activity_Wishlist.refresh();
        }
    }

    public static void getData() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        new Async2().execute(Activity_Main.target_cat, G.target, G.target_code, String.valueOf(myNum));

        mImageSlider.clear();
        final LinearLayout desc_list_exp = (LinearLayout) findViewById(R.id.desc_list_exp);
        final LinearLayout attrib_list_exp = (LinearLayout) findViewById(R.id.attrib_list_exp);

        relativeTopic = (LinearLayout) findViewById(R.id.relativeTopic);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        LinearLayout manufacturer_btn = (LinearLayout) findViewById(R.id.manufacturer_btn);
        LinearLayout manufacturer_layout = (LinearLayout) findViewById(R.id.manufacturer_layout);
        TextView manufacturer_btn_text = (TextView) findViewById(R.id.manufacturer_btn_text);
        manufacturer_btn_text.setTypeface(Activity_Main.SANS);
        TextView manufacturer_btn_text1 = (TextView) findViewById(R.id.manufacturer_btn_text1);
        manufacturer_btn_text1.setTypeface(Activity_Main.SANS);

        manufacturer_btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_Manufacturer.target = manufacturerTarget;
                Activity_Main.name = manufacturerName;
                Activity_Main.logo = manufacturerLogo;
                Directions.checkActivity(Activity_Manufacturer.target);
                selectedTab = 0;
            }
        });
        manufacturer_btn_text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity_Manufacturer.target = manufacturerTarget;
                Activity_Main.name = manufacturerName;
                Activity_Main.logo = manufacturerLogo;
                Directions.checkActivity(Activity_Manufacturer.target);
                selectedTab = 1;
            }
        });

        desc = (WebView) findViewById(R.id.desc);
        desc_list = (LinearLayout) findViewById(R.id.desc_list);
        description = (TextView) findViewById(R.id.description);
        attrib_list = (LinearLayout) findViewById(R.id.attrib_list);
        color_layout = (LinearLayout) findViewById(R.id.color_layout);
        price_layout = (LinearLayout) findViewById(R.id.price_layout);
        main_ll = (LinearLayout) findViewById(R.id.main_ll);
        attribute = (TextView) findViewById(R.id.attribute);
        txt_model = (TextView) findViewById(R.id.txt_model);
        relate = (TextView) findViewById(R.id.relate);
        care = (TextView) findViewById(R.id.care);
        no_related = (TextView) findViewById(R.id.no_related);
        product_color = (TextView) findViewById(R.id.product_color);
        txt_manufacturer = (TextView) findViewById(R.id.txt_manufacturer);
        txt_tel = (TextView) findViewById(R.id.txt_tel);
        txt_price = (TextView) findViewById(R.id.txt_price);
        txt_mobile = (TextView) findViewById(R.id.txt_mobile);
        txt_no_tel = (TextView) findViewById(R.id.txt_no_tel);
        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        txt3 = (TextView) findViewById(R.id.txt3);
        call = (ImageView) findViewById(R.id.call);
        compare = (ImageView) findViewById(R.id.compare);
        btnBack = (ImageView) findViewById(R.id.btnBack);
        btn_like = (ImageView) findViewById(R.id.btn_like);
        btn_in_room = (ImageView) findViewById(R.id.btn_in_room);
        btn_share = (ImageView) findViewById(R.id.btn_share);
        txt_model.setTypeface(Activity_Main.SANS);
        txt_manufacturer.setTypeface(Activity_Main.SANS);
        txt_tel.setTypeface(Activity_Main.SANS);
        txt_mobile.setTypeface(Activity_Main.SANS);
        txt_price.setTypeface(Activity_Main.SANS);
        txt1.setTypeface(Activity_Main.SANS);
        txt2.setTypeface(Activity_Main.SANS);
        txt3.setTypeface(Activity_Main.SANS);
        relate.setTypeface(Activity_Main.SANS);
        care.setTypeface(Activity_Main.SANS);
        txt_no_tel.setTypeface(Activity_Main.SANS);
        product_color.setTypeface(Activity_Main.SANS);
        description.setTypeface(Activity_Main.SANS);
        no_related.setTypeface(Activity_Main.SANS);
        attribute.setTypeface(Activity_Main.SANS);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );


        x = G.pricePref.getBoolean("price_data", true);
        if (x) {
            price_layout.setVisibility(View.VISIBLE);
        } else if (!x) {
            price_layout.setVisibility(View.GONE);
        }
        m = G.producerPref.getBoolean("producer_data", true);
        if (m) {
            manufacturer_btn.setVisibility(View.VISIBLE);
            manufacturer_layout.setVisibility(View.VISIBLE);
            params.setMargins(0, 0, 0, 175);
            main_ll.setLayoutParams(params);
        } else if (!m) {
            manufacturer_btn.setVisibility(View.GONE);
            manufacturer_layout.setVisibility(View.GONE);
            params.setMargins(0, 0, 0, 0);
            main_ll.setLayoutParams(params);
        }

        favPrefEdit = G.favoritePref.edit();
        favNamePrefEdit = G.favoriteNamePref.edit();
        favPicPrefEdit = G.favoritPicPref.edit();
        favImagePrefEdit = G.favoritImagePref.edit();


        boolean rb1 = G.favoritePref.getBoolean("rb1", false);
        if (!rb1) {

            try {
                favPrefEdit.putString("Favorite", ObjectSerializer.serialize(G.array));
            } catch (IOException e) {
                e.printStackTrace();
            }
            favPrefEdit.commit();
            favImagePrefEdit.putBoolean("rb1", true);
            favImagePrefEdit.commit();
        }
//        ArrayList<String> currentTasks = new ArrayList<String>();
//
//        try {
//            currentTasks = (ArrayList<String>) ObjectSerializer.deserialize(G.favoritePref.getString("Favorite", ObjectSerializer.serialize(new ArrayList<String>())));
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        String s = G.favoritePref.getString("Favorite", null);
//        setName = G.favoriteNamePref.getStringSet("FavoriteName", null);
//        setPic = G.favoritPicPref.getStringSet("FavoritePic", null);
//        sample = new ArrayList<String>(set);
//        sampleName = new ArrayList<String>(setName);
//        samplePic = new ArrayList<String>(setPic);


//        Log.d("mohammad", btn_like.getDrawable().toString());
        btn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liked == true) {
                    liked = false;
                    btn_like.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));

                    try {
                        G.array = (ArrayList<String>) ObjectSerializer.deserialize(G.favoritePref.getString("Favorite", ObjectSerializer.serialize(new ArrayList<String>())));
                        G.array.remove(productTarget);
                        G.array.remove(productName);
                        G.array.remove(tomb);

                        Log.d("mohammad", G.array.toString());

//                    set = new HashSet<String>();
//                    set.addAll(G.array);
                        favPrefEdit.putString("Favorite", ObjectSerializer.serialize(G.array));
                        favPrefEdit.commit();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
//                    G.nameArray.remove(productName);
//                    setName = new HashSet<String>();
//                    setName.addAll(G.nameArray);
//                    favNamePrefEdit.putStringSet("FavoriteName", setName);
//                    favNamePrefEdit.commit();
////                    Log.d("mohammmad", String.valueOf(G.array.size()));
//
//                    G.picArray.remove(tomb);
//                    setPic = new HashSet<String>();
//                    setPic.addAll(G.picArray);
//                    favPicPrefEdit.putStringSet("FavoritePic", setPic);
//                    favPicPrefEdit.commit();
                } else {
                    liked = true;
                    btn_like.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_border_red_24dp));

                    try {
                        G.array = (ArrayList<String>) ObjectSerializer.deserialize(G.favoritePref.getString("Favorite", ObjectSerializer.serialize(new ArrayList<String>())));
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    G.array.add(productTarget);
                    G.array.add(productName);
                    G.array.add(tomb);

                    Log.d("mohammad", G.array.toString());
//                    G.mainArray.add(G.array);
//                    set = new HashSet<String>();
//                    set.addAll(G.array);
                    try {
                        favPrefEdit.putString("Favorite", ObjectSerializer.serialize(G.array));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    favPrefEdit.commit();
//                    Log.d("mohammad", set.toString());

//                    Log.d("set size", String.valueOf(set.size()));
//                    Log.d("productname", manufacturerTarget);


//                    G.nameArray.add(productName);
//                    setName = new HashSet<String>();
//                    setName.addAll(G.nameArray);
//                    favNamePrefEdit.putStringSet("FavoriteName", setName);
//                    favNamePrefEdit.commit();


//                    G.picArray.add(tomb);
//                    setPic = new HashSet<String>();
//                    setPic.addAll(G.picArray);
//                    favPicPrefEdit.putStringSet("FavoritePic", setPic);
//                    favPicPrefEdit.commit();
//                    if (sample.contains(manufacturerTarget)) {
//                        Log.d("mohammmad", String.valueOf(G.array.size()));
//                    }
                }

            }
        });


        final String[] PERMISSIONS = {Manifest.permission.CAMERA};


        desc_list_exp.setVisibility(View.GONE);
        attrib_list_exp.setVisibility(View.GONE);
        txt_no_tel.setVisibility(View.GONE);

        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = shareContent;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "صفحه محصول در وب سایت:");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (G.isWishlist == true) {
                    Activity_Wishlist.refresh();
                }
                finish();
            }
        });
        btn_in_room.setVisibility(View.GONE);
        btn_in_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (!hasPermissions(getApplicationContext(), PERMISSIONS)) {
                        ActivityCompat.requestPermissions(Activity_Products.this, PERMISSIONS, PERMISSION_ALL);
                        Toast.makeText(Activity_Products.this, "دسترسی لازم برای ذخیره سازی صادر نشده است", Toast.LENGTH_LONG).show();
                    }
                    if (hasPermissions(getApplicationContext(), PERMISSIONS)) {
                        intent = new Intent(Activity_Products.this, Activity_Camera_sdk21.class);
                        Activity_Products.this.startActivity(intent);
                    }

                } else {
                    intent = new Intent(Activity_Products.this, Activity_Camera_sdk21.class);
                    Activity_Products.this.startActivity(intent);
                }
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mob = txt_mobile.getText().toString();
                String phone = txt_tel.getText().toString();
                String phone_number = null;
                if (mob.toString() != "null") {
                    phone_number = mob;
                    Intent call = new Intent(Intent.ACTION_DIAL);
                    call.setData(Uri.parse("tel:" + phone_number));
                    startActivity(call);
                } else if (phone.toString() != "null" && mob.toString() == "null") {
                    phone_number = phone;
                    Intent call = new Intent(Intent.ACTION_DIAL);
                    call.setData(Uri.parse("tel:" + phone_number));
                    startActivity(call);
                } else if (phone.toString() == "null" && mob == "null") {
                }
            }
        });
        compare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_Products.this, Activity_Compare.class);
                Activity_Products.this.startActivity(intent);
            }
        });

// view pager (onCreate)

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


// Colors (recyclerView)
        for (int i = 0; i < 3; i++) {
            Struct struct = new Struct();
            struct.strThird = "color" + i;
            structs.add(struct);
        }
        RecyclerView recyclerViewColors = (RecyclerView) findViewById(R.id.colors);
        RecyclerView.Adapter adapter_colors = new Adapter_Recycler(this, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                String name = structs.get(position).strThird;
//                Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        recyclerViewColors.setAdapter(adapter_colors);
        recyclerViewColors.setLayoutManager(manager);
        recyclerViewColors.removeItemDecoration(null);
        recyclerViewColors.setVisibility(View.GONE);
        color_layout.setVisibility(View.GONE);

// Related Items (recyclerView)

        RecyclerView recyclerViewRelated = (RecyclerView) findViewById(R.id.related);

        adapter_related = new Adapter_Recycler(this, relatedStructs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = relatedStructs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, 7, false);

        RecyclerView.LayoutManager manager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
        recyclerViewRelated.setAdapter(adapter_related);
        recyclerViewRelated.setLayoutManager(manager1);
        recyclerViewRelated.removeItemDecoration(null);
        final ImageView desc_arrow = (ImageView) findViewById(R.id.desc_arrow);
        final ImageView attrib_arrow = (ImageView) findViewById(R.id.attrib_arrow);
        desc_arrow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));
        attrib_arrow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));


        desc_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (desc_list_exp.getVisibility() == View.GONE) {
                    desc_list_exp.setVisibility(View.VISIBLE);
                    desc_arrow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_black_24dp));

                } else {
                    desc_list_exp.setVisibility(View.GONE);
                    desc_arrow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));

                }
            }
        });
        attrib_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (attrib_list_exp.getVisibility() == View.GONE) {
                    attrib_list_exp.setVisibility(View.VISIBLE);
                    attrib_arrow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_up_black_24dp));
                } else {
                    attrib_list_exp.setVisibility(View.GONE);
                    attrib_arrow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_black_24dp));
                }
            }
        });

    }


    // View Pager
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_slider, container, false);
            final TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            final ImageView section_image = (ImageView) rootView.findViewById(R.id.section_image);

            Glide
                    .with(getContext())
                    .load(mImageSlider.get(getArguments().getInt(ARG_SECTION_NUMBER) - 1))
                    .placeholder(R.drawable.background_card)
                    .fitCenter()
                    .into(section_image);

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent view = new Intent(getActivity(), Activity_ImageSlider.class);
                    startActivity(view);
                }
            });
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // number of pages
            return mImageSlider.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }


// Expandable listView

    private static class GroupItem {
        String title;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
    }

    private static class ChildHolder {
        TextView title;
        TextView hint;
    }

    private static class GroupHolder {
        TextView title;
    }

    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.expandable_productchild, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.hint = (TextView) convertView.findViewById(R.id.textHint);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.hint.setText(item.hint);
            holder.title.setTypeface(Activity_Main.SANS);
            holder.hint.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public int getPositionForSection(int section) {
            return listView.getFlatListPosition(ExpandableListView
                    .getPackedPositionForGroup(section));
        }

        @Override
        public int getSectionForPosition(int position) {
            return ExpandableListView.getPackedPositionGroup(listView
                    .getExpandableListPosition(position));
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.expandable_productgroup, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewPager.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);

    }

    private class Async2 extends Webservice.PostClass {


        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(Activity_Products.this, R.style.MyTheme);
            progress.setCancelable(false);
            progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            Activity_Main.request = result;
            // Page Contents
            try {
                JSONObject data = new JSONObject(result);
                firstObject = data.getJSONObject("data");
                JSONObject secondObject = firstObject.getJSONObject("manufacturer_info");
                attribArray = firstObject.getJSONArray("attribute_groups");

                firstArray = firstObject.getJSONArray("images");
                for (int i = 0; i < firstArray.length(); i++) {
                    JSONObject items = firstArray.getJSONObject(i);
                    String imageSlider = items.getString("popup");

                    JSONObject imgs = firstArray.getJSONObject(0);
                    tomb = imgs.getString("popup");

                    mImageSlider.add(imageSlider);
                }

                shareContent = firstObject.getString("share_text");
                productName = firstObject.getString("name");
                productTarget = firstObject.getString("target");

                String product_desc = firstObject.getString("description");
                String price = firstObject.getString("price");
                manufacturerName = secondObject.getString("name");
                manufacturerLogo = secondObject.getString("image");
                String manufacturerTel = secondObject.getString("tel");
                String manufacturerMobile = secondObject.getString("mob_tel");
                manufacturerTarget = secondObject.getString("target");
                env_image = firstObject.getString("env_image");
                if (env_image.toString().equals("false")) {
                    btn_in_room.setVisibility(View.GONE);
                } else if (env_image.toString() != "false") {
                    btn_in_room.setVisibility(View.VISIBLE);
                }

                txt_model.setText(productName);
                txt_manufacturer.setText(manufacturerName);
                txt_tel.setText(manufacturerTel);
                txt_mobile.setText(manufacturerMobile);
                txt_price.setText(price);
                if (txt_price.getText().toString().equals("false")) {
                    price_layout.setVisibility(View.GONE);
                }
                desc.loadData("<html dir=\"rtl\" lang=\"\"><body>" + product_desc + "</body></html>", "text/html; charset=utf-8", "UTF-8");

//
                G.nameArray = (ArrayList<String>) ObjectSerializer.deserialize(G.favoritePref.getString("Favorite", ObjectSerializer.serialize(new ArrayList<String>())));
                if (G.nameArray.contains(productTarget)) {
                    btn_like.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_border_red_24dp));
                    Log.d("mohammmad", "yes");
                    Log.d("mohammmad", productTarget);
                    liked = true;
                } else {
                    Log.d("mohammmad", "no");
                    btn_like.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
                    liked = false;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (txt_mobile.getText().toString() == "null" && txt_tel.getText() == "null") {
                txt_mobile.setVisibility(View.GONE);
                txt_tel.setVisibility(View.GONE);
                txt_no_tel.setVisibility(View.VISIBLE);
                txt_no_tel.setText("ثبت نشده");
                call.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button_disabled));
            }

            // Related Items (recyclerView)
            try {
                other_products = firstObject.getJSONArray("related_products");
                String relatedChecker = firstObject.get("related_products").toString();
                if (relatedChecker.equals("false")) {
                    relativeTopic.setVisibility(View.GONE);
                    relativeLayout.setVisibility(View.GONE);
                    no_related.setText("محصول مشابه پیدا نشد!");
                } else {
                    no_related.setVisibility(View.GONE);
                    for (int l = 0; l < other_products.length(); l++) {
                        Struct struct = new Struct();
                        JSONObject relatedProducts = other_products.getJSONObject(l);
                        struct.strFirst = relatedProducts.getString("name");
                        struct.strSecond = relatedProducts.getString("price");
                        struct.strThird = relatedProducts.getString("target");
                        struct.image = relatedProducts.getString("thumb");
                        relatedStructs.add(struct);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                relativeTopic.setVisibility(View.GONE);
                relativeLayout.setVisibility(View.GONE);
                no_related.setText("محصول مشابه پیدا نشد!");
            }
            adapter_related.notifyDataSetChanged();
            mSectionsPagerAdapter.notifyDataSetChanged();

            items = new ArrayList<GroupItem>();
            try {
                for (int i = 0; i < attribArray.length(); i++) {
                    JSONObject attribs = attribArray.getJSONObject(i);
                    GroupItem item = new GroupItem();
                    item.title = attribs.getString("name");
                    JSONArray attribsChild = attribs.getJSONArray("attribute");

                    for (int j = 0; j < attribsChild.length(); j++) {
                        JSONObject childs = attribsChild.getJSONObject(j);
                        ChildItem child = new ChildItem();
                        child.title = childs.getString("name");
                        child.hint = childs.getString("text");
                        item.items.add(child);
                    }
                    items.add(item);
                }
                adapterExp = new ExampleAdapter(Activity_Products.this);
                adapterExp.setData(items);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            listView = (AnimatedExpandableListView) findViewById(R.id.attrib);
            listView.setGroupIndicator(null);
            listView.setChildIndicator(null);
            listView.setDividerHeight(2);
            listView.setAdapter(adapterExp);
            for (int k = 0; k < attribArray.length(); k++) {
                listView.expandGroup(k);
            }
            listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    if (listView.isGroupExpanded(groupPosition)) {
//                        listView_com.collapseGroupWithAnimation(groupPosition);
                    } else {
//                        listView_com.expandGroupWithAnimation(groupPosition);
                    }
                    return true;
                }

            });
            adapterExp.notifyDataSetChanged();
            Utility.setListViewHeightBasedOnChildren(listView);


        }

    }

    private void expand() {
        int sa = attribArray.length();
        for (int k = 0; k < attribArray.length(); k++) {
            listView.expandGroup(k);
        }
        Utility.setListViewHeightBasedOnChildren(listView);

    }

    public void goToAttract() {
        Intent intent = new Intent(Activity_Products.this, Activity_Products.class);
        Activity_Products.this.startActivity(intent);
        getFragmentManager().executePendingTransactions();
        finish();

    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
