package ir.uncode.tabs;


import android.support.v7.app.AppCompatActivity;


public class EnhancedActivity extends AppCompatActivity {

    @Override
    protected void onResume() {
        G.currentActivity = this;
        super.onResume();
    }
}
