package ir.uncode.tabs;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;


public class Fragment_Category extends Fragment {
    private ArrayList<Struct> structs = new ArrayList<>();
    private ArrayList<Struct> structs1 = new ArrayList<>();
    private ArrayList<Struct> structs2 = new ArrayList<>();
    private ArrayList<Struct> structs3 = new ArrayList<>();

    private int Tab = 1;
    public static ArrayList bbbi = new ArrayList();
    public static String name;

    public Fragment_Category() {
        // Required empty public constructor
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);

        void addTab(TabLayout.Tab tab);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ArrayList aaaa = new ArrayList();
        ArrayList bbbb = new ArrayList();
        ArrayList cccc = new ArrayList();
        ArrayList dddd = new ArrayList();

        ArrayList aaai = new ArrayList();
        ArrayList ccci = new ArrayList();
        ArrayList dddi = new ArrayList();

        ArrayList aaaj = new ArrayList();
        ArrayList bbbj = new ArrayList();
        ArrayList cccj = new ArrayList();
        ArrayList dddj = new ArrayList();


        try {
            JSONArray cats = Splash_Screen.cat_array;
            JSONObject cat0 = cats.getJSONObject(0);
            JSONArray a_cat0 = cat0.getJSONArray("items");
            JSONObject cat1 = cats.getJSONObject(1);
            JSONArray a_cat1 = cat1.getJSONArray("items");
            JSONObject cat2 = cats.getJSONObject(2);
            JSONArray a_cat2 = cat2.getJSONArray("items");
            JSONObject cat3 = cats.getJSONObject(3);
            JSONArray a_cat3 = cat3.getJSONArray("items");

            for (int a = 0; a < a_cat0.length(); a++) {
                JSONObject aa = a_cat0.getJSONObject(a);
                String aaa = aa.getString("name");
                String aai = aa.getString("image");
                String aaj = aa.getString("target");
                aaaa.add(aaa);
                aaai.add(aai);
                aaaj.add(aaj);
            }
            for (int b = 0; b < a_cat1.length(); b++) {
                JSONObject bb = a_cat1.getJSONObject(b);
                String bbb = bb.getString("name");
                String bbi = bb.getString("image");
                String bbj = bb.getString("target");
                bbbb.add(bbb);
                bbbi.add(bbi);
                bbbj.add(bbj);
            }
            for (int c = 0; c < a_cat2.length(); c++) {
                JSONObject cc = a_cat2.getJSONObject(c);
                String ccc = cc.getString("name");
                String cci = cc.getString("image");
                String ccj = cc.getString("target");
                cccc.add(ccc);
                ccci.add(cci);
                cccj.add(ccj);
            }
            for (int d = 0; d < a_cat3.length(); d++) {
                JSONObject dd = a_cat3.getJSONObject(d);
                String ddd = dd.getString("name");
                String ddi = dd.getString("image");
                String ddj = dd.getString("target");
                dddd.add(ddd);
                dddi.add(ddi);
                dddj.add(ddj);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int h = 0; h < aaaa.size(); h++) {
            Struct struct = new Struct();
            struct.strFirst = (String) aaaa.get(h);
            struct.image = (String) aaai.get(h);
            struct.strThird = (String) aaaj.get(h);
            structs.add(struct);

        }
        for (int i = 0; i < bbbb.size(); i++) {
            Struct struct = new Struct();
            struct.strFirst = (String) bbbb.get(i);
            struct.image = (String) bbbi.get(i);
            struct.strThird = (String) bbbj.get(i);
            structs1.add(struct);
        }
        for (int j = 0; j < cccc.size(); j++) {
            Struct struct = new Struct();
            struct.strFirst = (String) cccc.get(j);
            struct.image = (String) ccci.get(j);
            struct.strThird = (String) cccj.get(j);
            structs2.add(struct);
        }
        for (int k = 0; k < dddd.size(); k++) {
            Struct struct = new Struct();
            struct.strFirst = (String) dddd.get(k);
            struct.image = (String) dddi.get(k);
            struct.strThird = (String) dddj.get(k);
            structs3.add(struct);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        TextView cat_head = (TextView) rootView.findViewById(R.id.cat_head);
        TextView cat_head1 = (TextView) rootView.findViewById(R.id.cat_head1);
        TextView cat_head2 = (TextView) rootView.findViewById(R.id.cat_head2);
        TextView cat_head3 = (TextView) rootView.findViewById(R.id.cat_head3);

        cat_head.setTypeface(Activity_Main.SANS);
        cat_head1.setTypeface(Activity_Main.SANS);
        cat_head2.setTypeface(Activity_Main.SANS);
        cat_head3.setTypeface(Activity_Main.SANS);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        RecyclerView recyclerView1 = (RecyclerView) rootView.findViewById(R.id.recyclerView1);
        RecyclerView recyclerView2 = (RecyclerView) rootView.findViewById(R.id.recyclerView2);
        RecyclerView recyclerView3 = (RecyclerView) rootView.findViewById(R.id.recyclerView3);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView1.setNestedScrollingEnabled(false);
        recyclerView2.setNestedScrollingEnabled(false);
        recyclerView3.setNestedScrollingEnabled(false);

        RecyclerView.Adapter adapter = new Adapter_Recycler(G.context, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);
        RecyclerView.Adapter adapter1 = new Adapter_Recycler(G.context, structs1, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs1.get(position).strThird;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);
        RecyclerView.Adapter adapter2 = new Adapter_Recycler(G.context, structs2, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs2.get(position).strThird;
                Directions.checkActivity(name);

            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);
        RecyclerView.Adapter adapter3 = new Adapter_Recycler(G.context, structs3, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs3.get(position).strThird;
                Directions.checkActivity(name);

            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);

        recyclerView.setAdapter(adapter);
        recyclerView1.setAdapter(adapter1);
        recyclerView2.setAdapter(adapter2);
        recyclerView3.setAdapter(adapter3);

        RecyclerView.LayoutManager manager = new GridLayoutManager(G.context, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        RecyclerView.LayoutManager manager1 = new GridLayoutManager(G.context, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        RecyclerView.LayoutManager manager2 = new GridLayoutManager(G.context, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        RecyclerView.LayoutManager manager3 = new GridLayoutManager(G.context, 2) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        recyclerView.setLayoutManager(manager);
        recyclerView1.setLayoutManager(manager1);
        recyclerView2.setLayoutManager(manager2);
        recyclerView3.setLayoutManager(manager3);

//        RecyclerView.OnItemTouchListener disabler = new RecyclerViewDisabler();
////        RecyclerView.OnScrollListener disabler1 = new RecyclerViewDisabler();
//        recyclerView.addOnItemTouchListener(disabler);        // disables scolling
//        recyclerView1.addOnItemTouchListener(disabler);
//        recyclerView2.addOnItemTouchListener(disabler);        // disables scolling
//        recyclerView3.addOnItemTouchListener(disabler);
////        recyclerView.addOnScrollListener(disabler1);
//// do stuff while scrolling is disabled
////        recyclerView.removeOnItemTouchListener(disabler);
//
        return rootView;
    }

//    public class RecyclerViewDisabler implements RecyclerView.OnItemTouchListener {
//
//
//        @Override
//        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
//
//        }
//
//        @Override
//        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
//            return false;
//        }
//
//        @Override
//        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//        }
//    }

    public class CustomGridLayoutManager extends LinearLayoutManager {
        private boolean isScrollEnabled = false;

        public CustomGridLayoutManager(Context context) {
            super(context);
        }

        public void setScrollEnabled(boolean flag) {
            this.isScrollEnabled = flag;
        }

        @Override
        public boolean canScrollVertically() {
            //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
            return isScrollEnabled && super.canScrollVertically();
        }
    }

    private class Async2 extends Webservice.PostClass {


        @Override
        protected void onPreExecute() {
            // Activity 1 GUI stuff
        }

        @Override
        protected void onPostExecute(String result) {
            Activity_Main.request = result;
            goToAttract();
        }
    }

    public void goToAttract() {
        Intent intent = new Intent(getActivity(), Activity_ProductsList.class);
        startActivity(intent);
        getFragmentManager().executePendingTransactions();
    }

}