package ir.uncode.tabs;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Poweruser on 08/10/2016.
 */
public class Activity_Wishlist extends Activity {
    private RecyclerView wishRecycler;
    private static Adapter_Recycler wishRecyclerAdapter;
    private LinearLayoutManager wishRecyclerManager;
    public static ArrayList<Struct> structs = new ArrayList<>();
    private String name;
    private String first;
    private String second;
    private String img;
    private SharedPreferences.Editor favPrefEdit;
    private ImageView btnBack;
    private ImageView btn_like;
    private ImageView btn_in_room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        G.isWishlist = true;
        favPrefEdit = G.favoritePref.edit();


        btnBack = (ImageView) findViewById(R.id.btnBack);
        btn_like = (ImageView) findViewById(R.id.btn_like);
        btn_in_room = (ImageView) findViewById(R.id.btn_in_room);
        btn_like.setVisibility(View.GONE);
        btn_in_room.setVisibility(View.GONE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                G.isWishlist = false;
                structs.clear();
                finish();
            }
        });
//        set = G.favoritePref.getStringSet("Favorite", null);
//        setName = G.favoriteNamePref.getStringSet("FavoriteName", null);
//        setPic = G.favoritPicPref.getStringSet("FavoritePic", null);
//        sample = new ArrayList<String>(set);
//        sampleName = new ArrayList<String>(setName);
//        samplePic = new ArrayList<String>(setPic);


        try {
            G.nameArray = (ArrayList<String>) ObjectSerializer.deserialize(G.favoritePref.getString("Favorite", ObjectSerializer.serialize(new ArrayList<String>())));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
//        int s = sample.size();
//        Log.d("mohammad", String.valueOf(s));
        for (int i = 0; i < G.nameArray.size(); i += 3) {
            Struct struct = new Struct();
            struct.strSecond = G.nameArray.get(i);
            struct.strFirst = G.nameArray.get(i + 1);
            struct.image = G.nameArray.get(i + 2);
            Log.d("mohammad", G.nameArray.get(i));
            Log.d("mohammad", G.nameArray.get(i + 1));
            Log.d("mohammad", G.nameArray.get(i + 2));
            structs.add(struct);
        }

        wishRecycler = (RecyclerView) findViewById(R.id.wishRecycler);
        wishRecyclerAdapter = new Adapter_Recycler(this, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                name = structs.get(position).strSecond;
                Directions.checkActivity(name);
            }

            @Override
            public void onItemClick(int position) {
                try {
                    first = structs.get(position).strFirst;
                    second = structs.get(position).strSecond;
                    img = structs.get(position).image;

                    G.array = (ArrayList<String>) ObjectSerializer.deserialize(G.favoritePref.getString("Favorite", ObjectSerializer.serialize(new ArrayList<String>())));
                    G.array.remove(first);
                    G.array.remove(second);
                    G.array.remove(img);

                    Log.d("mohammad", G.array.toString());

                    favPrefEdit.putString("Favorite", ObjectSerializer.serialize(G.array));
                    favPrefEdit.commit();

                    structs.clear();
                    for (int i = 0; i < G.array.size(); i += 3) {
                        Struct struct = new Struct();
                        struct.strSecond = G.array.get(i);
                        struct.strFirst = G.array.get(i + 1);
                        struct.image = G.array.get(i + 2);
                        Log.d("mohammad", G.array.get(i));
                        Log.d("mohammad", G.array.get(i + 1));
                        Log.d("mohammad", G.array.get(i + 2));
                        structs.add(struct);
                    }
                    wishRecyclerAdapter.notifyDataSetChanged();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }, 13, true);

        wishRecyclerManager = new GridLayoutManager(getApplicationContext(), 2);
        wishRecycler.setAdapter(wishRecyclerAdapter);
        wishRecycler.setLayoutManager(wishRecyclerManager);
        wishRecycler.removeItemDecoration(null);
    }

    public static void refresh() {
        structs.clear();
        for (
                int i = 0;
                i < G.array.size(); i += 3) {
            Struct struct = new Struct();
            struct.strSecond = G.array.get(i);
            struct.strFirst = G.array.get(i + 1);
            struct.image = G.array.get(i + 2);
            Log.d("mohammad", G.array.get(i));
            Log.d("mohammad", G.array.get(i + 1));
            Log.d("mohammad", G.array.get(i + 2));
            structs.add(struct);
        }
        wishRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        G.isWishlist = false;
        structs.clear();
    }
}
