package ir.uncode.tabs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.idunnololz.widgets.AnimatedExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;


public class Fragment_manufacturers extends Fragment {
    public ArrayList<Struct> structs = new ArrayList<>();
    private int Tab = 3;
    RecyclerView.Adapter adapter;
    private Dialog dialog;
    private AnimatedExpandableListView listView;
    private ExampleAdapter exp_adapter;
    private String name;
    private Boolean hasChild;
    private int previousGroup;
    private int cat;
    private RecyclerView recyclerView;
    private ProgressDialog progress;
    private int lastExpandedPosition = -1;
    private ExpandableListView lv;
    private TextView section_label;
    private String sort_name;
    private TextView sort_type;
    private boolean checked = false;

    public Fragment_manufacturers() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//DEFINE DIALOG BOX
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sort_list);
        dialog.setCancelable(true);

//PAGE CONTENT (MANUFACTURERS)
        try {
            JSONObject manufacturer = Splash_Screen.OBJECTS.getJSONObject("manufacturer");
            JSONArray data = manufacturer.getJSONArray("data");

            for (int i = 0; i < data.length(); i++) {
                Struct struct = new Struct();
                JSONObject jitems = data.getJSONObject(i);
                struct.strFirst = jitems.getString("name");
                struct.strSecond = jitems.getString("slogan");
                struct.image = jitems.getString("image");
                struct.strThird = jitems.getString("target");
                structs.add(struct);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
//EXPANDABLE LISTVIEW DATA (SORT DIALOG)
        List<GroupItem> items = new ArrayList<GroupItem>();
        try {
            for (int h = 0; h < Splash_Screen.SORT_NAMES.size(); h++) {
                GroupItem item = new GroupItem();
                item.title = (String) Splash_Screen.SORT_NAMES.get(h);
                item.titleHint = (String) Splash_Screen.SORT_TARGETS.get(h);
                JSONObject object_menu_item = Splash_Screen.sort_final_array.getJSONObject(h);
                JSONArray object_menu_childs = object_menu_item.getJSONArray("items");

                for (int j = 0; j < object_menu_childs.length(); j++) {
                    JSONObject object_menu_child = object_menu_childs.getJSONObject(j);
                    ChildItem child = new ChildItem();
                    child.title = object_menu_child.getString("name");
                    child.hint = object_menu_child.getString("target");
                    item.items.add(child);
                }
                items.add(item);
            }
            exp_adapter = new ExampleAdapter(getContext());
            exp_adapter.setData(items);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_manufacturers, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        //sort toolbar
        LinearLayout sort = (LinearLayout) rootView.findViewById(R.id.sort);
        TextView sort_txt = (TextView) rootView.findViewById(R.id.sort_txt);
        final ImageView clear_sort = (ImageView) rootView.findViewById(R.id.clear_sort);
        sort_type = (TextView) rootView.findViewById(R.id.sort_type);
        section_label = (TextView) rootView.findViewById(R.id.section_label);
        section_label.setTypeface(Activity_Main.SANS);
        clear_sort.setVisibility(View.GONE);
        sort_txt.setTypeface(Activity_Main.SANS);
        sort_type.setTypeface(Activity_Main.SANS);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
        clear_sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                structs.clear();
                adapter.notifyDataSetChanged();
                sort_type.setText("همه تولیدکننده ها");
                checked=false;
                exp_adapter.notifyDataSetChanged();
                clear_sort.setVisibility(View.GONE);
                try {
                    JSONObject manufacturer = Splash_Screen.OBJECTS.getJSONObject("manufacturer");
                    JSONArray data = manufacturer.getJSONArray("data");

                    for (int i = 0; i < data.length(); i++) {
                        Struct struct = new Struct();
                        JSONObject jitems = data.getJSONObject(i);
                        struct.strFirst = jitems.getString("name");
                        struct.strSecond = jitems.getString("slogan");
                        struct.image = jitems.getString("image");
                        struct.strThird = jitems.getString("target");
                        structs.add(struct);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        //sort button on dialog box
        TextView change_sort_mode = (TextView) dialog.findViewById(R.id.change_sort_mode);
        change_sort_mode.setTypeface(Activity_Main.SANS);
        change_sort_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (checked == true) {
                        clear_sort.setVisibility(View.VISIBLE);
                        sort_type.setText(sort_name);
                        StringTokenizer tokens = new StringTokenizer(name, ":");
                        G.target = "";
                        G.target_code = "";
                        G.target = tokens.nextToken();
                        G.target_code = tokens.nextToken();
                        new Async2().execute("manufacturer_list", G.target, G.target_code, String.valueOf(1));
                        dialog.dismiss();
                    }
                    else {
                        Toast.makeText(Activity_Main.CONTEXT, "فیلتری انتخاب نشده!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(Activity_Main.CONTEXT, "فیلتری انتخاب نشده!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//RECYCLERVIEW (VIEW GROUP)
        adapter = new Adapter_Recycler(G.context, structs, new OnItemListener() {
            @Override
            public void onItemSelect(int position) {
                Activity_Manufacturer.target = structs.get(position).strThird;
                Activity_Main.name = structs.get(position).strFirst;
                Activity_Main.logo = structs.get(position).image;
                Activity_Products.selectedTab = 1;
                Directions.checkActivity(Activity_Manufacturer.target);
            }

            @Override
            public void onItemClick(int position) {

            }
        }, Tab, false);
        recyclerView.setAdapter(adapter);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(manager);
        recyclerView.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener((GridLayoutManager) manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                customLoadMoreDataFromApi(page);
            }
        });

//EXPANDABLE LISTVIEW (VIEW GROUP)
        listView = (AnimatedExpandableListView) dialog.findViewById(R.id.listView);
        listView.setAdapter(exp_adapter);
        listView.setDividerHeight(2);
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                String groupName = exp_adapter.getGroup(groupPosition).titleHint;
                Activity_Main.groupTitle = exp_adapter.getGroup(groupPosition).title;
                StringTokenizer tokens = new StringTokenizer(groupName, ":");
                G.target = "";
                G.target_code = "";
                G.target = tokens.nextToken();
                G.target_code = tokens.nextToken();

                if (G.target.equals("information_info")) {
                    Activity_Main.target_cat = "information_info";
                    Intent intent = new Intent(getActivity(), Activity_Information.class);
                    startActivity(intent);
                }

                if (listView.isGroupExpanded(groupPosition)) {
                    listView.collapseGroupWithAnimation(groupPosition);
                } else {
                    listView.expandGroupWithAnimation(groupPosition);

                }
                return true;
            }
        });
        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    listView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                return true;
            }
        });
        return rootView;

    }

    // ASYNC TASK OF SORT FEATURE
    private class Async2 extends Webservice.PostClass {

        @Override
        protected void onPreExecute() {
            structs.clear();
            adapter.notifyDataSetChanged();
            progress = new ProgressDialog(getContext(), R.style.MyTheme);
            progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress.setCancelable(true);
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            try {
                JSONObject manufacturer = new JSONObject(result);
                JSONArray data = manufacturer.getJSONArray("data");

                for (int i = 0; i < data.length(); i++) {
                    Struct struct = new Struct();
                    JSONObject items = data.getJSONObject(i);
                    struct.strFirst = items.getString("name");
                    struct.strSecond = items.getString("slogan");
                    struct.image = items.getString("image");
                    struct.strThird = items.getString("target");
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            adapter.notifyDataSetChanged();
            if (structs.size() == 0) {
                section_label.setText("فعلا اینجا هیچی نیست!");
            } else {
                section_label.setVisibility(View.GONE);
            }
        }
    }

    // ASYNCH TASK OF PAGINATION
    private class Async3 extends Webservice.PostClassNoCat {

        @Override
        protected void onPreExecute() {
            progress = new ProgressDialog(getContext(), R.style.MyTheme);
            progress.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress));
            progress.setCancelable(true);
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            try {
                JSONObject manufacturer = new JSONObject(result);
                JSONArray data = manufacturer.getJSONArray("data");

                for (int i = 0; i < data.length(); i++) {
                    Struct struct = new Struct();
                    JSONObject items = data.getJSONObject(i);
                    struct.strFirst = items.getString("name");
                    struct.strSecond = items.getString("slogan");
                    struct.image = items.getString("image");
                    struct.strThird = items.getString("target");
                    structs.add(struct);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            adapter.notifyDataSetChanged();

        }
    }


    public void customLoadMoreDataFromApi(int page) {
        new Async3().execute("manufacturer_list", String.valueOf(page + 1));
    }


    // EXPANDABLE LISTVIEW
    private static class GroupItem {
        String title;
        String titleHint;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
    }

    private static class ChildHolder {
        TextView title;
        TextView hint;
        RadioButton radio;
    }

    private static class GroupHolder {
        TextView title;
        TextView titleHint;
    }

    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        int selectedPosition = 0;
        int selectedGroupPosition = 0;

        @Override
        public View getRealChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.expandable_childitem_sort, parent, false);
                holder.radio = (RadioButton) convertView.findViewById(R.id.radio);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.hint = (TextView) convertView.findViewById(R.id.textHint);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            if (checked == false) {
                holder.radio.setChecked(false);

            }
            if (checked == true) {
                holder.radio.setChecked(groupPosition == selectedGroupPosition && childPosition == selectedPosition);
            }
            holder.radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checked = true;
                    selectedPosition = childPosition;
                    selectedGroupPosition = groupPosition;
                    notifyDataSetChanged();
                    if (name != null && !name.isEmpty() && !name.equals("null")) {
                        name = "";
                    }
                    name = exp_adapter.getChild(groupPosition, childPosition).hint;
                    sort_name = exp_adapter.getChild(groupPosition, childPosition).title;
                }
            });
            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);
            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public int getPositionForSection(int section) {
            return 0;
        }

        @Override
        public int getSectionForPosition(int position) {
            return 0;
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.expandable_groupitem_sort, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.titleHint = (TextView) convertView.findViewById(R.id.textTitleHint);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }
            holder.titleHint.setText(item.titleHint);
            holder.title.setText(item.title);
            holder.title.setTypeface(Activity_Main.SANS);

            if (getChildrenCount(groupPosition) == 0) {
                hasChild = false;
            } else {
                hasChild = true;
            }

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }
    }


}