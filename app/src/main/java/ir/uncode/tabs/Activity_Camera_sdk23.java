package ir.uncode.tabs;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.media.Image;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class Activity_Camera_sdk23 extends Activity implements OnTouchListener {
    private static ImageView background;
    private ImageView share;
    private ImageView size;
    private ImageView capture;
    private RelativeLayout ll;
    private static File file;
    public static int rotation;


    private static final String TAG = "Touch";
    // These matrices will be used to move and zoom image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();
    Matrix savedMatrix2 = new Matrix();

    // We can be in one of these 3 states
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // Remember some things for zooming
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;
    private float MAX_ZOOM;
    private float[] lastEvent;
    private float d;
    private float newRot;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        capture = (ImageView) findViewById(R.id.capture_img);
        share = (ImageView) findViewById(R.id.share_img);
        size = (ImageView) findViewById(R.id.x_img);
        share.setVisibility(View.GONE);
        size.setVisibility(View.GONE);
        background = (ImageView) findViewById(R.id.background_img);
        ll = (RelativeLayout) findViewById(R.id.root);
        ImageView imageView = (ImageView) findViewById(R.id.product_img);

        imageView.setOnTouchListener(this);
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction().replace(R.id.container, Activity_Camera_sdk23_Fragment.newInstance()).commit();
        }

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (share.getVisibility() == View.GONE) {
                    capture.setImageDrawable(getResources().getDrawable(R.drawable.ic_sd_card_black_24dp));
                    Activity_Camera_sdk23_Fragment.takePicture();
                    share.setVisibility(View.VISIBLE);
                    size.setVisibility(View.VISIBLE);
                } else if (share.getVisibility() == View.VISIBLE) {

//                    background.setImageDrawable(Activity_Camera_sdk23_Fragment.mImage);
                    capture.setVisibility(View.GONE);
                    share.setVisibility(View.GONE);
                    size.setVisibility(View.GONE);
                    Toast.makeText(Activity_Camera_sdk23.this, "Image Saved", Toast.LENGTH_SHORT).show();
                    Random num = new Random();
                    ll.setDrawingCacheEnabled(true); //CamView OR THE NAME OF YOUR LAYOUR
                    ll.buildDrawingCache(true);
                    Bitmap bmp = Bitmap.createBitmap(ll.getDrawingCache());
                    ll.setDrawingCacheEnabled(false); // clear drawing cache
                    String root = Environment.getExternalStorageDirectory().toString();
                    File myDir = new File(root + "/req_images");
                    myDir.mkdirs();
                    Random generator = new Random();
                    int n = 10000;
                    n = generator.nextInt(n);
                    String fname = "Image-" + n + ".jpg";
                    file = new File(myDir, fname);
//                    Log.i(TAG, "" + file);
                    if (file.exists())
                        file.delete();
                    try {
                        FileOutputStream out = new FileOutputStream(file);
                        bmp.compress(Bitmap.CompressFormat.JPEG, 100, out);
                        out.flush();
                        out.close();
                        background.setImageBitmap(null);
                        Activity_Camera_sdk21_SurfaceView.bmp1.recycle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    capture.setVisibility(View.VISIBLE);
                    share.setVisibility(View.VISIBLE);
                    size.setVisibility(View.VISIBLE);
                    capture.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button_disabled));

                }

            }
        });

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        ImageView view = (ImageView) v;
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;

        // Dump touch event to log
        dumpEvent(event);

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: //first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                Log.d(TAG, "mode=DRAG");
                mode = DRAG;
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                lastEvent = new float[4];
                lastEvent[0] = event.getX(0);
                lastEvent[1] = event.getX(1);
                lastEvent[2] = event.getY(0);
                lastEvent[3] = event.getY(1);
                d = rotation(event);
                break;

            case MotionEvent.ACTION_UP: //first finger lifted
            case MotionEvent.ACTION_POINTER_UP: //second finger lifted
                mode = NONE;
                Log.d(TAG, "mode=NONE");
                break;


            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    // ...
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x, event.getY()
                            - start.y);
                } else if (mode == ZOOM && event.getPointerCount() == 2) {
                    float newDist = spacing(event);
                    matrix.set(savedMatrix);
                    if (newDist > 10f) {
                        scale = newDist / oldDist;
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                    if (lastEvent != null) {
                        newRot = rotation(event);
                        float r = newRot - d;
                        matrix.postRotate(r, view.getMeasuredWidth() / 2,
                                view.getMeasuredHeight() / 2);
                    }
                }
                break;

        }
        // Perform the transformation
        view.setImageMatrix(matrix);

        return true; // indicate event was handled

    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);

        return (float) Math.toDegrees(radians);
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);

    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);

    }


    /**
     * Show an event in the LogCat view, for debugging
     */

    private void dumpEvent(MotionEvent event) {
        String names[] = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP) {
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }

        sb.append("[");

        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }

        sb.append("]");
        Log.d(TAG, sb.toString());

    }

    @Override
    public void onBackPressed() {
        if (share.getVisibility() == View.GONE) {
            finish();
        } else if (share.getVisibility() == View.VISIBLE) {
            capture.setImageDrawable(getResources().getDrawable(R.drawable.ic_photo_camera_black_24dp));
            capture.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.circle_button));
            Activity_Camera_sdk21_SurfaceView.mCamera.startPreview();
            Activity_Camera_sdk21_SurfaceView.bmp1.recycle();
            Activity_Camera_sdk21_SurfaceView.bmp1 = null;
            background.setImageBitmap(null);
            share.setVisibility(View.GONE);
            size.setVisibility(View.GONE);
        }
    }

}
