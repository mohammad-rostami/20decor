package ir.uncode.tabs;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import java.io.File;
import java.util.ArrayList;


public class G extends Application {

    public static Context context;
    public static LayoutInflater inflater;
    public static ArrayList<Struct> structs;
    public static String target;
    public static String target_code;
    public static String product_target;
    public static String product_target_code;
    public static Activity currentActivity;
    public static Boolean isUpdate = false;
    public static String route;
    public static String dataTarget;
    public static String dataCode;
    public static ArrayList<String> backUp = new ArrayList();
    public static SharedPreferences pricePref;
    public static SharedPreferences producerPref;
    public static SharedPreferences favoritePref;
    public static SharedPreferences favoriteNamePref;
    public static SharedPreferences favoritImagePref;
    public static SharedPreferences favoritPicPref;
    public static SharedPreferences settingPref;
    public static ArrayList<String> array;
    public static ArrayList<String> nameArray;
    public static ArrayList<String> picArray;
    public static ArrayList<ArrayList<String>> mainArray;
    public static Boolean isWishlist=false;

    @Override
    public void onCreate() {
        super.onCreate();
        startService(new Intent(this, RunService.class));

        pricePref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        producerPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        favoritePref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        favoriteNamePref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        favoritImagePref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        favoritPicPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        settingPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        array = new ArrayList();
        nameArray = new ArrayList();
        picArray = new ArrayList();
        mainArray = new ArrayList();

        structs = new ArrayList<>();
        context = getApplicationContext();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }
}
