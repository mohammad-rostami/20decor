package ir.uncode.tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.crash.FirebaseCrash;

import java.io.IOException;

import io.karim.MaterialTabs;

public class Activity_Main extends EnhancedActivity implements Fragment_Category.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener {

    public static Context CONTEXT;
    public static Typeface SANS;
    public static String request;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private LinearLayout search_view;
    private ViewPager mViewPager;
    private static DrawerLayout drawer;
    private Activity activity;
    private TextView tabText;
    private boolean added;
    public static String target_cat;
    public static String groupTitle;
    public static String logo;
    public static String name;

    public Activity_Main() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseCrash.report(new Exception("My first Android non-fatal error"));
        setContentView(R.layout.activity_main);


        boolean rb1 = G.favoritePref.getBoolean("rb1", false);
        if (!rb1) {

            try {
                G.favoritePref.edit().putString("Favorite", ObjectSerializer.serialize(G.array));
            } catch (IOException e) {
                e.printStackTrace();
            }
            G.favoritePref.edit().commit();
            G.favoritImagePref.edit().putBoolean("rb1", true);
            G.favoritImagePref.edit().commit();
        }

        boolean rb2 = G.settingPref.getBoolean("rb2", false);
        if (!rb2) {

            G.pricePref.edit().putBoolean("price_data", true);
            G.producerPref.edit().putBoolean("producer_data", true);
            G.pricePref.edit().commit();
            G.producerPref.edit().commit();

            G.settingPref.edit().putBoolean("rb2", true);
            G.settingPref.edit().commit();
        }


        ImageView btnSort = (ImageView) findViewById(R.id.btnSort);
        btnSort.setVisibility(View.GONE);

        CONTEXT = getApplicationContext();
        SANS = Typeface.createFromAsset(getAssets(), "sans.ttf");
        TextView pageName = (TextView) findViewById(R.id.pageName);
        pageName.setText("خانه");
        pageName.setTypeface(SANS);

// Check For update (check static variable)
        if (Splash_Screen.NEW_UPDATE == "yes") {
            DialogClass_Update cdd = new DialogClass_Update(Activity_Main.this);
            cdd.show();
        } else {
        }
// Show update Dialog
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AppBarLayout topBar = (AppBarLayout) findViewById(R.id.topBar);
                topBar.bringToFront();
            }
        }, 1000);

// Creating adapter that return the fragment
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

// Setup ViewPager with adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(2);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mViewPager.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

// Tabs
        MaterialTabs tabs = (MaterialTabs) findViewById(R.id.tabs);
        tabs.setViewPager(mViewPager);
        tabs.setTypefaceSelected(SANS);
        tabs.setTypefaceUnselected(SANS);


// Navigation Drawer
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        ImageView btnNavigation = (ImageView) findViewById(R.id.btnNavigation);
        btnNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.RIGHT);
            }
        });
        Fragment squadFragment = new Fragment_Navigation();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.nav_view, squadFragment, null);
        fragmentTransaction.commit();


// Search
        search_view = (LinearLayout) findViewById(R.id.search_view);
        TextView search_txt = (TextView) findViewById(R.id.search_txt);
        search_txt.setTypeface(SANS);

        search_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_Main.this, Activity_Search.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    public Activity getActivity() {
        return activity;
    }

    public boolean isAdded() {
        return added;
    }


    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        String tabTitles[] = new String[]{"دکور", "تولیدکنندگان", "ویترین", "دسته بندی"};
        Context context;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Fragment_Decor();

                case 1:
                    return new Fragment_manufacturers();
                case 2:
                    return new Fragment_Showcase();

                case 3:
                    return new Fragment_Category();
            }
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        public View getTabView(int position, View v, boolean selected) {
            if (v == null) {
                View tab = LayoutInflater.from(Activity_Main.this).inflate(R.layout.custom_tab, null);
            }
            tabText = (TextView) v.findViewById(R.id.custom_text);
            tabText.setText(tabTitles[position]);
            tabText.setTypeface(SANS);

            return v;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
//            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    //@Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        return true;
    }

    @Override
    public void addTab(TabLayout.Tab tab) {
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            return false;
        }
    }
    public static void closeDrawer(){
            drawer.closeDrawer(GravityCompat.END);
    }
}
